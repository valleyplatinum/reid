--
-- MySQL database dump
-- Created by DBManage class, Power By yanue. 
-- http://yanue.net 
--
-- 主机: localhost
-- 生成日期: 2017 年  08 月 08 日 10:22
-- MySQL版本: 5.6.17
-- PHP 版本: 5.5.12

--
-- 数据库: `thinkphp`
--

-- -------------------------------------------------------

--
-- 表的结构tp_admin
--

DROP TABLE IF EXISTS `tp_admin`;
CREATE TABLE `tp_admin` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `radom` varchar(6) DEFAULT NULL,
  `log_num` int(10) NOT NULL COMMENT '登录次数',
  `log_ip` varchar(30) NOT NULL,
  `log_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_admin
--

INSERT INTO `tp_admin` VALUES('1','admin','0299b78762247a66e9624ef690b75078','xe8m27','26','::1','1502154657');
--
-- 表的结构tp_category
--

DROP TABLE IF EXISTS `tp_category`;
CREATE TABLE `tp_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `parent` int(10) NOT NULL DEFAULT '0',
  `children` varchar(255) NOT NULL,
  `display` int(1) NOT NULL DEFAULT '1',
  `listorder` int(10) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `wap_url` varchar(255) NOT NULL,
  `web_title` varchar(255) NOT NULL,
  `web_description` varchar(255) NOT NULL,
  `web_text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_category
--

--
-- 表的结构tp_cc
--

DROP TABLE IF EXISTS `tp_cc`;
CREATE TABLE `tp_cc` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_cc
--

INSERT INTO `tp_cc` VALUES('1','/2017/08/04/5983ed2d0de8e.jpg');
--
-- 表的结构tp_dbhistory
--

DROP TABLE IF EXISTS `tp_dbhistory`;
CREATE TABLE `tp_dbhistory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_dbhistory
--

--
-- 表的结构tp_email
--

DROP TABLE IF EXISTS `tp_email`;
CREATE TABLE `tp_email` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `fuwuqi` varchar(200) NOT NULL,
  `duankou` varchar(20) NOT NULL,
  `fajianren` varchar(255) NOT NULL,
  `you` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_email
--

INSERT INTO `tp_email` VALUES('1','smtp.163.com','465','18634411966@163.com','18634411966@163.com','Gbb5663027');
--
-- 表的结构tp_ip
--

DROP TABLE IF EXISTS `tp_ip`;
CREATE TABLE `tp_ip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(100) NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`,`time`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_ip
--

INSERT INTO `tp_ip` VALUES('34','::1','1501058930');
INSERT INTO `tp_ip` VALUES('33','::1','1501057982');
INSERT INTO `tp_ip` VALUES('31','::1','1501056982');
INSERT INTO `tp_ip` VALUES('30','::1','1501056781');
INSERT INTO `tp_ip` VALUES('29','::1','1501056376');
INSERT INTO `tp_ip` VALUES('28','::1','1501056313');
INSERT INTO `tp_ip` VALUES('27','::1','1501056246');
INSERT INTO `tp_ip` VALUES('26','::1','1501056183');
INSERT INTO `tp_ip` VALUES('25','::1','1501056110');
INSERT INTO `tp_ip` VALUES('24','::1','1501055985');
--
-- 表的结构tp_mobile
--

DROP TABLE IF EXISTS `tp_mobile`;
CREATE TABLE `tp_mobile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(15) NOT NULL,
  `time` int(10) NOT NULL,
  `sms` int(6) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1注册2找密',
  PRIMARY KEY (`id`),
  KEY `mobile` (`mobile`,`time`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_mobile
--

INSERT INTO `tp_mobile` VALUES('5','18634411966','1501056110','144745','0','1');
INSERT INTO `tp_mobile` VALUES('4','18634411966','1501055985','141049','0','1');
INSERT INTO `tp_mobile` VALUES('6','18634411966','1501056183','161614','0','1');
INSERT INTO `tp_mobile` VALUES('7','18634411966','1501056246','199158','0','1');
INSERT INTO `tp_mobile` VALUES('10','18634411966','1501058930','548631','0','1');
--
-- 表的结构tp_water
--

DROP TABLE IF EXISTS `tp_water`;
CREATE TABLE `tp_water` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `open` int(1) NOT NULL COMMENT '1开启0关闭',
  `img` varchar(100) NOT NULL COMMENT '图片',
  `word` varchar(255) NOT NULL COMMENT '文字',
  `loca` int(1) NOT NULL COMMENT '位置',
  `style` int(1) NOT NULL COMMENT '1图片2文字',
  `opacity` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_water
--

INSERT INTO `tp_water` VALUES('1','1','59853139a5641.png',' http://www.gengbinbin.com ','9','2','80');
--
-- 表的结构tp_web
--

DROP TABLE IF EXISTS `tp_web`;
CREATE TABLE `tp_web` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `banquan` varchar(255) DEFAULT NULL,
  `icp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 tp_web
--

INSERT INTO `tp_web` VALUES('1','ThinkPHP','ThinkPHP','ThinkPHP','Copyright 2016 by ThinkPHP ®. All Rights Reserved.','沪ICP备08101622号');
