<?php 
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 * 功能说明：后台公共文件。
 */

function lang($model){
//    if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){
//        $m=M($model);
//    }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){
//        $m=M($model,'en_');
//    }
    if($_SERVER['SERVER_NAME'] =='testing.com'){
        $m=M($model);
    }elseif($_SERVER['SERVER_NAME'] =='en.testing.com'){
        $m=M($model,'en_');
    }
    return $m;
}

//清空缓存
function clean($model){
    if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){
        $m=S($model,null);
    }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){
        $m=S($model.'1',null);
    }
    return $m;
}

//建立缓存
function build($model,$array){
    if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){
        $m=S($model,$array);
    }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){
        $m=S($model.'1',$array);
    }
    return $m;
}

//读取缓存
function read($model){
    if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){
        $m=S($model);
    }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){
        $m=S($model.'1');
    }
    return $m;
}