<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Home\Controller;
use Think\Controller;
class ProductController extends AppController {
    public function index(){
    	$Classify = read('Classify');
    	if(!$Classify){
    		$Classify=lang('Classify');
    		$Classify=$Classify->order('id asc')->select();
    		build('Classify',$Classify);
    		$this->assign('Classify',$Classify);
    	}else{
    		$this->assign('Classify',$Classify);
    	}

        if(!I('get.pid')){
            $arr=lang('Product')->where('type2='.$Classify['0']['id'])->order('id asc')->select();
            $this->assign('fa',$Classify['0']['type']);
        }else{
            foreach ($Classify as $key => $value) {
                if(I('get.pid')==$value['id']){
                    $this->assign('fa',$value['type']);
                    break;
                }
            }
            $arr=lang('Product')->where('type2='.I('get.pid'))->order('id asc')->select();
        }
        $this->assign('arr',$arr);

        if(ismobile()){
            $this->display('wap_index');
        }else{
            $this->display();
        }
    }
}