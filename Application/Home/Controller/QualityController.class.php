<?php
/**
 * Author: anxin
 * Since: 2019-07-03 11:06
 * description: 品质管理
 */
namespace Home\Controller;
use Think\Controller;
class QualityController extends AppController {
    // 品质管理-质量管理与控制
    public function index(){
//    	$About = read('About');
//    	if(!$About){
//    		$About=lang('About');
//    		$About=$About->find();
//    		build('About',$About);
//    		$this->assign('About',$About);
//    	}else{
//    		$this->assign('About',$About);
//    	}

        if(ismobile()){
            $this->display('wap_index');
        }else{
            $this->display();
        }
    }

    // 品质管理-质量控制流程图
    public function process(){
        if(ismobile()){
            $this->display('wap_process');
        }else{
            $this->display();
        }
    }
    // 品质管理-生产工序简介
    public function itroduction(){
        if(ismobile()){
            $this->display('wap_itroduction');
        }else{
            $this->display();
        }
    }
}