<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Home\Controller;
use Think\Controller;
class IndexController extends AppController {
    public function index(){
    	$Banner = read('Banner');
    	if(!$Banner){
    		$Banner=lang('Banner');
    		$Banner=$Banner->order('id asc')->select();
    		build('Banner',$Banner);
    		$this->assign('Banner',$Banner);
    	}else{
    		$this->assign('Banner',$Banner);
    	}

    	$About = read('About');
    	if(!$About){
    		$About=lang('About');
    		$About=$About->find();
    		build('About',$About);
    		$this->assign('About',$About);
    	}else{
    		$this->assign('About',$About);
    	}

    	$Classify = read('Classify');
    	if(!$Classify){
    		$Classify=lang('Classify');
    		$Classify=$Classify->order('id asc')->select();
    		build('Classify',$Classify);
    		$this->assign('Classify',$Classify);
    	}else{
    		$this->assign('Classify',$Classify);
    	}

    	$Field = read('Field');
    	if(!$Field){
    		$Field=lang('Field');
    		$Field=$Field->order('id asc')->select();
    		build('Field',$Field);
    		$this->assign('Field',$Field);
    	}else{
    		$this->assign('Field',$Field);
    	}

    	$Qual = read('Qual');
    	if(!$Qual){
    		$Qual=lang('Qual');
    		$Qual=$Qual->order('id asc')->select();
    		build('Qual',$Qual);
    		$this->assign('Qual',$Qual);
    	}else{
    		$this->assign('Qual',$Qual);
    	}

        if(ismobile()){
            $this->display('wap_index');
        }else{
            $this->display();
        }
        
    }

    public function no(){
        if(ismobile()){
            $this->display('wap_no');
        }else{
            $this->display();
        }
    }

    public function search(){
        if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){
            $this->search_cn();
        }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){
            $this->search_en();
        }
    }

    public function search_cn(){
        switch (urldecode(I('get.kw'))){
            case '电话':
                $this->redirect('Contact/index');
                break;
            case '传真':
                $this->redirect('Contact/index');
                break;
            case '邮箱':
                $this->redirect('Contact/index');
                break;
            case '地址':
                $this->redirect('Contact/index');
                break;
            case '产品':
                $this->redirect('Product/index');
                break;
            default:
                $map['text']=array('like','%'.urldecode(I('get.kw')).'%');
                $arr=lang('Product')->where($map)->find();
                if($arr){
                    $this->redirect('Product/index');
                }else{
                    $this->redirect('Index/no');
                }
                break;
        }
    }

    public function search_en(){
        switch (urldecode(I('get.kw'))){
            case 'tel':
                $this->redirect('Contact/index');
                break;
            case 'fax':
                $this->redirect('Contact/index');
                break;
            case 'email':
                $this->redirect('Contact/index');
                break;
            case 'address':
                $this->redirect('Contact/index');
                break;
            case 'product':
                $this->redirect('Product/index');
                break;
            default:
                $map['text']=array('like','%'.urldecode(I('get.kw')).'%');
                $arr=lang('Product')->where($map)->find();
                if($arr){
                    $this->redirect('Product/index');
                }else{
                    $this->redirect('Index/no');
                }
                break;
        }
    }
}