<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Home\Controller;
use Think\Controller;
header("Content-type:text/html;charset=utf-8");

class AppController extends Controller {
	public function _initialize(){
		Vendor('lang');
		$lang = new \lang();
		$this->assign('lang',$lang);
		
		$this->assign('admin_tou','山西瑞德');

		if(!session('?normally_open')){
			$sms=array_merge(range(0,9),range('a','z'),range('A','Z'));
			shuffle($sms);
			$str=join('',array_slice($sms,0,32));
			session('normally_open',$str);
		}

		$Web = read('Web');
		if(!$Web){
			$Web=lang('Web');
			$Web=$Web->find();
			build('Web',$Web);
			$this->assign('Web',$Web);
		}else{
			$this->assign('Web',$Web);
		}

		$Contact = read('Contact');
		if(!$Contact){
			$Contact=lang('Contact');
			$Contact=$Contact->find();
			build('Contact',$Contact);
			$this->assign('Contact',$Contact);
		}else{
			$this->assign('Contact',$Contact);
		}

		$Category = read('Category');
		if(!$Category){
			$Category = lang('Category');
		    $arr=$Category->where('display=1')->order('listorder asc')->select();
		    build('Category',$arr);
		    $this->assign('Category',$arr);
		}else{
			$this->assign('Category',$Category);
		}

		foreach ($Category as $key => $value) {
			$url1=explode('/',$value['url']);
			if($url1['0'] == CONTROLLER_NAME){
				$web_title=$value['web_title'];
				$web_description=$value['web_description'];
				$web_text=$value['web_text'];
				$title=$value['title'];
				$this->assign('title',$title);
				break;
			}
		}
		if(CONTROLLER_NAME=='Index'){
			$web_title=$Web['title'];
			$web_description=$Web['keywords'];
			$web_text=$Web['description'];
		}
		$this->assign('web_title',$web_title);
		$this->assign('web_description',$web_description);
		$this->assign('web_text',$web_text);


	}
}
?>