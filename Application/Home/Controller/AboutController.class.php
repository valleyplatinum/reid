<?php
/**
 * Author: anxin
 * Since: 2019-07-03 11:06
 * description: 走进瑞德
 */
namespace Home\Controller;
use Think\Controller;
class AboutController extends AppController {
    // 走进瑞德-公司简介
    public function index(){
    	$About = read('About');
    	if(!$About){
    		$About=lang('About');
    		$About=$About->find();
    		build('About',$About);
    		$this->assign('About',$About);
    	}else{
    		$this->assign('About',$About);
    	}

        if(ismobile()){
            $this->display('wap_index');
        }else{
            $this->display();
        }
    }

    // 走进瑞德-企业文化
    public function culture(){
        if(ismobile()){
            $this->display('wap_culture');
        }else{
            $this->display();
        }
    }

    // 走进瑞德-组织机构
    public function organization(){
        if(ismobile()){
            $this->display('wap_organization');
        }else{
            $this->display();
        }
    }

    // 走进瑞德-资质证书
    public function certifications(){
        if(ismobile()){
            $this->display('wap_certifications');
        }else{
            $this->display();
        }
    }

    // 走进瑞德-总经理致辞
    public function speech(){
        if(ismobile()){
            $this->display('wap_speech');
        }else{
            $this->display();
        }
    }
}