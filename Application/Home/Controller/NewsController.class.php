<?php
/**
 * Author: anxin
 * Since: 2019-07-03 13:37
 * description: 走进瑞德-媒体中心
 */
namespace Home\Controller;
use Think\Controller;
class NewsController extends AppController {
    public function index(){
    	$News=lang('News');
        $count = $News->count();
        $page = new \Think\Page($count,10);
        $page->setConfig('prev','上一页');
        $page->setConfig('next','下一页');
        $show = $page->show();
        $arr=$News->limit($page->firstRow.','.$page->listRows)->order('id desc')->select();
        $this->assign('page',$show);
        $this->assign('arr',$arr);

        if(ismobile()){
            $this->display('wap_index');
        }else{
            $this->display();
        }
    }

    public function show(){
        $arr=lang('News')->where('id='.I('get.id'))->find();
        $this->assign('arr',$arr);

        $num=$arr['num']+1;
        lang('News')->save(array('id'=>I('get.id'),'num'=>"$num"));

        $arr2=lang('News')->where('id!='.I('get.id'))->order('id desc')->limit(10)->select();
        $this->assign('arr2',$arr2);
        if(ismobile()){
            $this->display('wap_show');
        }else{
            $this->display();
        }
    }
}