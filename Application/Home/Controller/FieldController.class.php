<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Home\Controller;
use Think\Controller;
class FieldController extends AppController {
    public function index(){
    	$Next = read('Next');
    	if(!$Next){
    		$Next=lang('Next');
    		$Next=$Next->order('id asc')->select();
    		build('Next',$Next);
    		$this->assign('Next',$Next);
    	}else{
    		$this->assign('Next',$Next);
    	}

        $Field = read('Field');
        if(!$Field){
            $Field=lang('Field');
            $Field=$Field->order('id asc')->select();
            build('Field',$Field);
            $this->assign('Field',$Field);
        }else{
            $this->assign('Field',$Field);
        }

        if(ismobile()){
            $this->display('wap_index');
        }else{
            $this->display();
        }
    }

    public function find(){
        $Next = read('Next');
        if(!$Next){
            $Next=lang('Next');
            $Next=$Next->order('id asc')->select();
            build('Next',$Next);
        }

        $Field = read('Field');
        if(!$Field){
            $Field=lang('Field');
            $Field=$Field->order('id asc')->select();
            build('Field',$Field);
        }

        foreach ($Next as $key => $value) {
            if($value['id']==I('post.id')){
                $title=$value['title'];
                $text=$value['text'];
                break;
            }
        }
        $img='';
        foreach ($Field as $key => $value) {
            if($value['type']==I('post.id')){
                if(ismobile()){
                    $img.="<div class='swiper-slide'><img src='/Uploads/img$value[img]' alt='' /><p>$value[title]</p></div>";
                }else{
                    $img.="<div class='swiper-slide'><div class='img'><img src='/Uploads/img$value[img]' alt='' /></div><div class='title'>$value[title]</div></div>";
                }
            }
        }
        echo json_encode(array('code'=>"ok",'title'=>"$title",'text'=>"$text",'img'=>"$img"));exit;
    }
}