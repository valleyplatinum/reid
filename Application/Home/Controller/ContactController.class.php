<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Home\Controller;
use Think\Controller;
class ContactController extends AppController {
    public function index(){
        $Contact = read('Contact');
        if(!$Contact){
            $Contact=lang('Contact');
            $Contact=$Contact->find();
            build('Contact',$Contact);
            $this->assign('Contact',$Contact);
        }else{
            $this->assign('Contact',$Contact);
        }
        if(ismobile()){
            $this->display('wap_index');
        }else{
            $this->display();
        }
    }
}