<?php 
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 * 功能说明：后台公共文件。
 */

function lang($model){
    if(session('admin_lang')=='cn'){
        $m=M($model);
    }else{
        $m=M($model,'en_');
    }
    return $m;
}

//清空缓存
function clean($model){
    if(session('admin_lang')=='cn'){
        $m=S($model,null);
    }else{
        $m=S($model.'1',null);
    }
    return $m;
}

//建立缓存
function build($model,$array){
    if(session('admin_lang')=='cn'){
        $m=S($model,$array);
    }else{
        $m=S($model.'1',$array);
    }
    return $m;
}

//读取缓存
function read($model){
    if(session('admin_lang')=='cn'){
        $m=S($model);
    }else{
        $m=S($model.'1');
    }
    return $m;
}