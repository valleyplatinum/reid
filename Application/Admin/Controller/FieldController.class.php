<?php
namespace Admin\Controller;
use Think\Controller;

class FieldController extends AppController
{
    public function next(){
        $arr=lang('Field')->order('id asc')->select();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function add(){
        $this->display();
    }

    public function insert(){
        $data['img']=I('post.img');
        $data['title']=I('post.title');
        $data['type']=I('post.type');
        lang('Field')->add($data);
        $this->cache();
        echo 1;
    }

    public function edit(){
        $arr=lang('Field')->where('id='.I('get.id'))->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function update(){
        $data['img']=I('post.img');
        $data['title']=I('post.title');
        $data['type']=I('post.type');
        $data['id']=I('post.id');
        lang('Field')->save($data);
        $this->cache();
        echo 1;
    }

    public function delete(){
        lang('Field')->where('id='.I('post.id'))->delete();
        $this->cache();
        echo 1;
    }

    function cache(){
        clean('Field',null);
        $arr=lang('Field')->order('id asc')->select();
        build('Field',$arr);
    }

    public function classify(){
        $arr=lang('Next')->order('id asc')->select();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function classify_update(){
        for ($i=1; $i <=8 ; $i++) { 
            lang('Next')->save(array('id'=>$i,'title'=>I('post.title'.$i),'text'=>I('post.text'.$i)));
        }
        clean('Next',null);
        $arr=lang('Next')->order('id asc')->select();
        build('Next',$arr);
        echo 1;
    }

}
