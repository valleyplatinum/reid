<?php
namespace Admin\Controller;
use Think\Controller;

class TechController extends AppController
{
    public function index(){
        if(I('type') && I('type') != 0){
            $map['type']=I('type');
        }elseif(I('type') && I('type') == 0){
            $map='';
        }else{
            $map='';
        }

        $arr=lang('Tech')->where($map)->order('id asc')->select();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function add(){
        $this->display();
    }

    public function insert(){
        $data['img']=I('post.img');
        $data['title']=I('post.title');
        $data['type']=I('post.type');
        lang('Tech')->add($data);
        $this->cache();
        echo 1;
    }

    public function edit(){
        $arr=lang('Tech')->where('id='.I('get.id'))->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function update(){
        $data['img']=I('post.img');
        $data['title']=I('post.title');
        $data['type']=I('post.type');
        $data['id']=I('post.id');
        lang('Tech')->save($data);
        $this->cache();
        echo 1;
    }

    public function delete(){
        lang('Tech')->where('id='.I('post.id'))->delete();
        $this->cache();
        echo 1;
    }

    function cache(){
        clean('Tech',null);
        $arr=lang('Tech')->order('id asc')->select();
        build('Tech',$arr);
    }

}
