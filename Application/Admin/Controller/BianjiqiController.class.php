<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Admin\Controller;
use Think\Controller;
class BianjiqiController extends Controller {

    public function _initialize(){
        ini_set('memory_limit', '2048M');
    }
    
    public function img(){
        $name=array_keys($_FILES);
        $dizhi=folder2();
        $config = array(
            'maxSize'    =>    0,
            'rootPath'   =>    $dizhi['0'],
            'saveName'   =>    array('uniqid',''),
            'exts'       =>    '',
            'autoSub'    =>    false,
            'replace'    =>    true,
        );
        $upload = new \Think\Upload($config);
        $info = $upload->upload(array($_FILES[$name['0']]));

        echo $dizhi['1'].$info['0']['savename'];
        exit;
    }

    //编辑器上传图片，未来修改url即可
    public function wangEditor(){
        if(session('admin_lang')=='cn'){
            $url='http://sx-ruide.com/';
        }else{
            $url='http://en.sx-ruide.com/';
        }

        $dizhi=folder();
        $config = array(
            'maxSize'    =>    0,               //1M
            'rootPath'   =>    $dizhi['0'],
            'saveName'   =>    array('uniqid',''),
            'autoSub'    =>    false,
            'replace'    =>    true,
        );
        $upload = new \Think\Upload($config);
        $info = $upload->upload(array($_FILES['wangEditorH5File']));

        if(!$info) {
            echo 'no';
            exit;
        }else{
            echo $url.'Uploads/bianjiqi'.$dizhi['1'].$info['0']['savename'];
        }
        exit;
    }

    public function img_all2(){
        if(session('admin_lang')=='cn'){
            $url='http://sx-ruide.com/';
        }else{
            $url='http://en.sx-ruide.com/';
        }
        $dizhi=folder2();
        $name=array_keys($_FILES);
        
        $config = array(
            'maxSize'    =>    0,               //1M
            'rootPath'   =>    $dizhi['0'],
            'saveName'   =>    array('uniqid',''),
            'autoSub'    =>    false,
            'replace'    =>    true,
        );
        $upload = new \Think\Upload($config);
        $info = $upload->upload(array($_FILES[$name['0']]));
        
        if(!$info) {
            echo 'no';
            exit;
        }else{
            $img=$url.'Uploads/img'.$dizhi['1'].$info['0']['savename'];
            
            $arr=lang('About')->where('id='.I('get.id'))->find();
            if($arr[$name['0']] == ''){
                lang('About')->save(array('id'=>I('get.id'),$name['0']=>"$img"));
                $oo=$img;
            }else{
                $oo=$arr[$name['0']].','.$img;
                lang('About')->save(array('id'=>I('get.id'),$name['0']=>"$oo"));
            }
            
            if($name['0']=='img1'){
                $type='img1';
            }else{
                $type='img2';
            }
            $oimg=explode(',',$oo);
            $cc='';
            foreach ($oimg as $key => $value) {
                $cc.="<div class='baotu' id='bao".$key."'><img src='".$value."' alt=''><div class='shanchu' index='".$value."' ids='".I('get.id')."' k='".$key."' type='".$type."'>删除</div></div>"; }
            
            echo $cc;

        }
        exit;
    }
}