<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Admin\Controller;
use Think\Controller;
class ContactController extends AppController {

    public function index(){
        $arr=lang('Contact')->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function update(){
        $Contact=lang('Contact');
        $Contact->save(array('company'=>I('post.company'),'addr'=>I('post.addr'),'tel'=>I('post.tel'),'fax'=>I('post.fax'),'email'=>I('post.email'),'name'=>I('post.name'),'tel2'=>I('post.tel2'),'qq'=>I('post.qq'),'email2'=>I('post.email2'),'id'=>1));
        clean('Contact',null);
        $brr=lang('Contact')->find();
        build('Contact',$brr);
        echo 1;
    }

}