<?php
namespace Admin\Controller;
use Think\Controller;

class ProductController extends AppController
{
    public function index(){
        $this->display();
    }

    public function classify(){
        if(I('type') && I('type') != 0){
            $map['type']=I('type');
        }elseif(I('type') && I('type') == 0){
            $map='';
        }else{
            $map='';
        }

        $Classify=lang('Classify');
        $count = $Classify->where($map)->count();
        $page = new \Think\Page($count,10);
        $page->setConfig('prev','上一页');
        $page->setConfig('next','下一页');
        $show = $page->show();
        $arr=$Classify->limit($page->firstRow.','.$page->listRows)->where($map)->order('id desc')->select();
        $this->assign('page',$show);
        $this->assign('arr',$arr);
        $this->display();
    }

    public function classify_add(){
        $this->display();
    }

    public function classify_insert(){
        $data['title']=I('post.title');
        $data['img']=I('post.img');
        $data['type']=I('post.type');
        lang('Classify')->add($data);
        $this->cache();
        echo 1;
    }

    public function classify_edit(){
        $arr=lang('Classify')->where('id='.I('get.id'))->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function classify_update(){
        $data['title']=I('post.title');
        $data['img']=I('post.img');
        $data['type']=I('post.type');
        $data['id']=I('post.id');
        lang('Classify')->save($data);
        $this->cache();
        echo 1;
    }

    public function classify_delete(){
        lang('Classify')->where('id='.I('post.id'))->delete();
        $this->cache();
        echo 1;
    }

    function cache(){
        clean('Classify',null);
        $arr=lang('Classify')->order('id asc')->select();
        build('Classify',$arr);
    }

    public function product(){
        if(I('get.type') && I('get.type') != 0){
            $map['type']=I('get.type');
        }elseif(I('get.type') && I('get.type') == 0){
            $map='';
        }else{
            $map='';
        }
        if(I('get.type2') && I('get.type2') != 0){
            $map['type2']=I('get.type2');
        }

        $Product=lang('Product');
        $count = $Product->where($map)->count();
        $page = new \Think\Page($count,20);
        $page->setConfig('prev','上一页');
        $page->setConfig('next','下一页');
        $show = $page->show();
        $arr=$Product->limit($page->firstRow.','.$page->listRows)->where($map)->order('id asc')->select();
        $this->assign('page',$show);

        if(I('get.type') && I('get.type') != 0){
            $data['type']=I('get.type');
        }elseif(I('get.type') && I('get.type') == 0){
            $data='';
        }else{
            $data='';
        }
        $brr=lang('Classify')->where($data)->order('id asc')->select();

        $this->assign('arr',$arr);
        $this->assign('brr',$brr);
        $this->display();
    }

    public function product_add(){
        $arr=lang('Classify')->where('type=1')->order('id asc')->select();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function product_insert(){
        $str=$_POST['text'];
        $str.'<br>';
        $text=nl2br($str);

        $data['title']=I('post.title');
        $data['text']=$text;
        $data['type']=I('post.type');
        $data['type2']=I('post.type2');
        lang('Product')->add($data);
        echo 1;
    }

    public function product_edit(){
        $arr=lang('Product')->where('id='.I('get.id'))->find();
        $brr=lang('Classify')->where('type='.$arr['type'])->order('id asc')->select();
        $this->assign('arr',$arr);
        $this->assign('brr',$brr);
        $this->display();
    }

    public function product_update(){
        $str=$_POST['text'];
        $str.'<br>';
        $text=nl2br($str);

        $data['title']=I('post.title');
        $data['text']=$text;
        $data['type']=I('post.type');
        $data['type2']=I('post.type2');
        $data['id']=I('post.id');
        lang('Product')->save($data);
        echo 1;
    }

    public function product_delete(){
        lang('Product')->where('id='.I('post.id'))->delete();
        echo 1;
    }

    public function ajax_classify(){
        $arr=lang('Classify')->where('type='.I('post.type'))->order('id asc')->select();
        $data='';
        foreach ($arr as $key => $value) {
            $data.="<option value='$value[id]'>$value[title]</option>";
        }
        echo $data;
    }

}
