<?php
namespace Admin\Controller;
use Think\Controller;

class BannerController extends AppController
{
    public function index(){
        $arr=lang('Banner')->order('id asc')->select();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function add(){
        $this->display();
    }

    public function insert(){
        $data['img']=I('post.img');
        $data['text1']=I('post.text1');
        $data['text2']=I('post.text2');
        lang('Banner')->add($data);
        $this->cache();
        echo 1;
    }

    public function edit(){
        $arr=lang('Banner')->where('id='.I('get.id'))->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function update(){
        $data['img']=I('post.img');
        $data['text1']=I('post.text1');
        $data['text2']=I('post.text2');
        $data['id']=I('post.id');
        lang('Banner')->save($data);
        $this->cache();
        echo 1;
    }

    public function delete(){
        lang('Banner')->where('id='.I('post.id'))->delete();
        $this->cache();
        echo 1;
    }

    function cache(){
        clean('Banner',null);
        $arr=lang('Banner')->order('id asc')->select();
        build('Banner',$arr);
    }

}
