<?php
namespace Admin\Controller;
use Think\Controller;

class NewsController extends AppController
{
    public function index(){
        $News=lang('News');
        $count = $News->count();
        $page = new \Think\Page($count,20);
        $page->setConfig('prev','上一页');
        $page->setConfig('next','下一页');
        $show = $page->show();
        $arr=$News->limit($page->firstRow.','.$page->listRows)->order('id desc')->select();
        $this->assign('page',$show);
        $this->assign('arr',$arr);
        $this->display();
    }

    public function add(){
        $this->display();
    }

    public function insert(){
        $data['title']=I('post.title');
        $data['description']=I('post.description');
        $data['text']=$_POST['text'];
        $data['time']=time();
        lang('News')->add($data);
        echo 1;
    }

    public function edit(){
        $arr=lang('News')->where('id='.I('get.id'))->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function update(){
        $data['title']=I('post.title');
        $data['description']=I('post.description');
        $data['text']=$_POST['text'];
        $data['id']=I('post.id');
        lang('News')->save($data);
        echo 1;
    }

    public function delete(){
        lang('News')->where('id='.I('post.id'))->delete();
        echo 1;
    }

}
