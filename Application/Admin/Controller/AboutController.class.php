<?php
namespace Admin\Controller;
use Think\Controller;

class AboutController extends AppController
{
    public function index(){
        $arr=lang('About')->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function del_img(){
        $arr=lang('About')->where('id='.I('post.id'))->find();
        $img=explode(',',$arr[I('post.type')]);
        foreach ($img as $key => $value) {
            if($value == I('post.img')){
                $k=$key;
                break;
            }
        }
        unset($img[$k]);
        $cimg=implode(',',$img);
        lang('About')->save(array('id'=>I('post.id'),I('post.type')=>"$cimg"));
        
        echo 1;
    }

    public function update(){
        $str=$_POST['left'];
        $str.'<br>';
        $left=nl2br($str);

        $str2=$_POST['right'];
        $str2.'<br>';
        $right=nl2br($str2);

        $data['left']=$left;
        $data['right']=$right;
        $data['index']=I('post.index');
        $data['img']=I('post.img');
        $data['id']=1;
        lang('About')->save($data);
        $this->cache();
        echo 1;
    }

    function cache(){
        clean('About',null);
        $arr=lang('About')->find();
        build('About',$arr);
    }

}
