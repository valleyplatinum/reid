<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Admin\Controller;
use Think\Controller;
class AdminController extends Controller {

    public function login(){
        $this->assign('admin_tou','山西瑞德');

        $Web=M('Web');
        $web=$Web->find();
        $this->assign('web',$web);
                
       	$this->display();
    }

    public function verify(){
        $config =    array(
            'length' => 4,
            'fontSize' => 40
        );
        $Verify = new \Think\Verify($config);
        $Verify->entry();
    }

    public function login_yan(){
        $Admin = M('Admin');
        $admin=$Admin->where('id=1')->find();

        if($admin['username'] != I('post.username')){
            echo 1;
            exit;
        }

        $password=$admin['password'];
        $radom=$admin['radom'];
        $po_password=md5(md5(trim(I('post.password'))).$radom);

        if($password != $po_password){
            echo 2;
            exit;
        }

        $verify = new \Think\Verify();
        $res = $verify->check(I('post.sms'));
        if($res==''){
            echo 3;
            exit;
        }

       
        $data['log_num']=$admin['log_num']+1;
        $data['log_ip']=$_SERVER['REMOTE_ADDR'];
        $data['log_time']=time();
        $data['id']=1;
        $Admin->save($data);

        session('admin_id',$admin['id']);
        session('admin_username',$admin['username']);
        session('admin_lang','cn');

        echo 4;
    }

    public function logout(){
        session('admin_id',null);
        session('admin_username',null);
        session('admin_lang',null);
        $this->redirect('Admin/login');
    }

    public function lang(){
        if(session('admin_lang')=='cn'){
            session('admin_lang',null);
            session('admin_lang','en');
        }else{
            session('admin_lang',null);
            session('admin_lang','cn');
        }
        echo 1;
    }

}