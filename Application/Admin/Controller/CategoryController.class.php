<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Admin\Controller;
use Think\Controller;
class CategoryController extends AppController {
    public function index(){
        $Category = lang('Category');
        $arr=$Category->order('listorder asc')->select();
        $this->assign('Category',$arr);

        $this->display();
    }

    public function add(){
        $this->display();
    }

    public function insert(){
        $Category = lang('Category');

        $data['title']=I('post.title');
        $data['display']=I('post.display');
        $data['url']=I('post.url');
        $data['wap_url']=I('post.wap_url');
        $data['web_title']=I('post.web_title');
        $data['web_description']=I('post.web_description');
        $data['web_text']=I('post.web_text');
        $Category->add($data);
        $this->cache();
        echo 1;
    }



    // public function insert_child(){
    //     $Category = M('Category');
        
    //     $data['title']=I('post.title');
    //     $data['display']=I('post.display');
    //     $data['url']=I('post.url');
    //     $data['wap_url']=I('post.wap_url');
    //     $data['web_title']=I('post.web_title');
    //     $data['web_description']=I('post.web_description');
    //     $data['web_text']=I('post.web_text');
    //     $data['parent']=I('post.parent');
    //     $Category->add($data);

    //     $last=$Category->order('id desc')->find();

    //     $arr=$Category->where('id='.I('post.parent'))->find();

    //     if($arr['children'] == ''){
    //         $child=$last['id'];
    //     }else{
    //         $child=$arr['children'].','.$last['id'];
    //     }

    //     $map['children']=$child;
    //     $map['id']=I('post.parent');
    //     $Category->save($map);
    //     $this->cache();
    //     echo 1;
    // }

    // public function addsan(){
    //     $Category = M('Category');
    //     $arr=$Category->where('id='.I('get.id'))->find();
    //     $this->assign('arr',$arr);
    //     $this->display();
    // }

    public function del1(){
        $Category = lang('Category');
        $arr=$Category->where('id='.I('post.id'))->find();

        if($arr['children'] == ''){
            $Category->delete(I('post.id'));
        }else{
            $map['id']=array('in',$arr['children']);
            $child=$Category->where($map)->select();
            foreach ($child as $key => $value) {
                if($value['children'] != ''){
                    $Category->delete($value['children']);
                }
            }
            $Category->delete($arr['children']);
            $Category->delete(I('post.id'));
        }

        $this->cache();
        
        echo 1;
    }

    // public function del2(){
    //     $Category = M('Category');
    //     $arr=$Category->where('id='.I('post.id'))->find();

    //     if($arr['children'] != ''){
    //         $Category->delete($arr['children']);
    //     }

    //     $parent=$Category->where("id=$arr[parent]")->find();
    //     $old=explode(',',$parent['children']);
    //     $key = array_search(I('post.id'), $old);
    //     if($key !== false){
    //         array_splice($old, $key, 1);
    //     }
    //     $orr=implode(',',$old);
        
    //     $Category->where("id=$arr[parent]")->setField(array('children'=>"$orr"));
    //     $Category -> delete(I('post.id'));

    //     $this->cache();
        
    //     echo 1;
    // }

    // public function del3(){
    //     $Category = M('Category');
    //     $arr=$Category->where('id='.I('post.id'))->find();

    //     $parent=$Category->where("id=$arr[parent]")->find();
    //     $old=explode(',',$parent['children']);
    //     $key = array_search(I('post.id'), $old);
    //     if($key !== false){
    //         array_splice($old, $key, 1);
    //     }
    //     $orr=implode(',',$old);
        
    //     $Category->where("id=$arr[parent]")->setField(array('children'=>"$orr"));
    //     $Category -> delete(I('post.id'));
    //     $this->cache();
        
    //     echo 1;
    // }

    public function listorder(){
        $Category = lang('Category');
        $pai=explode(',',I('post.pai'));
        $id1=explode(',',I('post.id'));

        array_pop($pai);
        $id=array_filter($id1);

        foreach ($id as $key => $value) {
            $arr=$Category->where("id=$value")->find();
            $Category->where("id=$value")->setField(array('listorder'=>$pai[$key]));
        }

        $crr=$Category->where("parent = 0")->select();
        foreach ($crr as $key => $valu) {
            if($valu['children'] != ''){
                $map['id'] = array('in',$valu['children']);
                $brr=$Category->where($map)->order('listorder asc')->select();
                    
                if($brr['0']['parent'] == $valu['id']){
                    $id='';
                    foreach ($brr as $key => $value) {
                        $id.=$value['id'].',';   
                    }
                    $xu=explode(',',$id);
                    array_pop($xu);
                    $xus=implode(',',$xu);
                    $Category->where("id=$valu[id]")->setField(array('children'=>"$xus"));
                }

                $data['id']=array('in',$valu['children']);
                $drr=$Category->where($data)->select();

                    foreach ($drr as $key => $valus) {
                        if($valus['children'] != ''){
                            $map2['id'] = array('in',$valus['children']);
                            $brr2=$Category->where($map2)->order('listorder asc')->select();
                                
                            if($brr2['0']['parent'] == $valus['id']){
                                $ids='';
                                foreach ($brr2 as $key => $value) {
                                    $ids.=$value['id'].',';   
                                }
                                $xu=explode(',',$ids);
                                array_pop($xu);
                                $xus=implode(',',$xu);
                                $Category->where("id=$valus[id]")->setField(array('children'=>"$xus"));
                            }
                        }
                    }
                
            }
        }
        $this->cache();
        echo 1;
    }

    public function edit(){
        $Category = lang('Category');
        $arr=$Category->where('id='.I('get.id'))->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function update(){
        $Category = lang('Category');
        
        $data['title']=I('post.title');
        $data['display']=I('post.display');
        $data['url']=I('post.url');
        $data['wap_url']=I('post.wap_url');
        $data['web_title']=I('post.web_title');
        $data['web_description']=I('post.web_description');
        $data['web_text']=I('post.web_text');
        $data['id']=I('post.id');
        $Category->save($data);

        $this->cache();
        echo 1;
    }

    function cache(){
        $Category = lang('Category');
        clean('Category',null);

        $brr=$Category->order('listorder asc')->select();
        build('Category',$brr);
    }

}