<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Admin\Controller;
use Think\Controller;
header("Content-type:text/html;charset=utf-8");

class AppController extends Controller {
	public function _initialize(){
		$this->assign('admin_tou','山西瑞德');
		
		$Web = read('Web');
		if(!$Web){
			$Web=lang('Web');
			$Web=$Web->find();
			build('Web',$Web);
			$this->assign('Web',$Web);
		}else{
			$this->assign('Web',$Web);
		}

		if(!session('?admin_id')){
			$this->redirect('Admin/login');
		}

	}
}
?>