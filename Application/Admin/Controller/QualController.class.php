<?php
namespace Admin\Controller;
use Think\Controller;

class QualController extends AppController
{
    public function index(){
        $arr=lang('Qual')->order('id asc')->select();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function add(){
        $this->display();
    }

    public function insert(){
        $data['img']=I('post.img');
        $data['title']=I('post.title');
        lang('Qual')->add($data);
        $this->cache();
        echo 1;
    }

    public function edit(){
        $arr=lang('Qual')->where('id='.I('get.id'))->find();
        $this->assign('arr',$arr);
        $this->display();
    }

    public function update(){
        $data['img']=I('post.img');
        $data['title']=I('post.title');
        $data['id']=I('post.id');
        lang('Qual')->save($data);
        $this->cache();
        echo 1;
    }

    public function delete(){
        lang('Qual')->where('id='.I('post.id'))->delete();
        $this->cache();
        echo 1;
    }

    function cache(){
        clean('Qual',null);
        $arr=lang('Qual')->order('id asc')->select();
        build('Qual',$arr);
    }

}
