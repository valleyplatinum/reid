<?php
/**
 * Author: Mr.Geng
 * Since: 2017-08-01 17:28
 * Alipay：gbb5663027@vip.qq.com(本人愿意接受来自各方面的捐赠)
 */
namespace Admin\Controller;
use Think\Controller;
class SystemController extends AppController {

    public function index(){
        $this->display();
    }

    public function update(){
        $Web=lang('Web');
        $Web->save(array('title'=>I('post.title'),'keywords'=>I('post.keywords'),'description'=>I('post.description'),'banquan'=>I('post.banquan'),'icp'=>I('post.icp'),'id'=>1));
        clean('Web',null);
        $brr=lang('Web')->find();
        build('Web',$brr);
        echo 1;
    }

    public function favicons(){
        $config = array(
            'maxSize'    =>    0,
            'rootPath'   =>    './Public/',
            'saveName'   =>    'favicon',
            'exts'       =>    array('ico'),
            'autoSub'    =>    false,
            'replace'    =>    true,
        );
        $upload = new \Think\Upload($config);
        $info = $upload->upload();
        if(!$info) {
            echo 'no';
        }else{
            echo 'yes';
        }
    }

    public function password(){
        $this->display();
    }

    public function update_password(){
        $Admin = M('Admin');

        $admin=$Admin->where('id=1')->find();
        $old_pass=$admin['password'];
        $old_radom=$admin['radom'];
        $po_password=md5(md5(trim(I('post.old_pass'))).$old_radom);

        if($old_pass != $po_password){
            echo 1;
            exit;
        }

        $shu=array_merge(range(0,9),range('a','z'),range('A','Z'));
        shuffle($shu);
        $str=join('',array_slice($shu,0,6));

        $password=md5(md5(trim(I('post.new_pass'))).$str);

        $data['password']=$password;
        $data['radom']=$str;
        $data['id']=1;
        $Admin->save($data);

        echo 2;
    }

}