<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/thinkphp/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/thinkphp/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/dist/css/wangEditor.min.css" />
<script type="text/javascript" src="/thinkphp/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/wangEditor/dist/js/wangEditor.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="/thinkphp" id="url">
<?php
 $ru=$_SERVER['REQUEST_URI']; $fru=explode('/',$ru); $ye=array_pop($fru); echo '<input type="hidden" value='.$ye.' id="now_ye">'; ?>

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($web['title']); ?></a>
	<span class="Hui-userbox">
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
						<li><a href="<?php echo U('System/img');?>">图片水印</a></li>
						<!-- <li><a href="<?php echo U('System/database');?>">数据库</a></li> -->
						<!-- <li><a href="<?php echo U('System/email');?>">邮箱设置</a></li> -->
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<style>
    .mt-20{margin-top:0}
    .f-14 a{float:left}
</style>
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 导航设置 <span class="c-gray en">&gt;</span> 导航设置
</nav>
<div class="pd-20 text-c">
    <div class="article-class-list cl mt-20">
        <span class="l"><a href="<?php echo U('Admin/Category/add');?>" class="btn btn-primary radius" style="margin-bottom:10px"><i class="icon-plus"></i> 添加一级导航</a></span>
        <table class="table table-border table-bordered table-hover table-bg">
            <thead>
                <tr class="text-c">
                    <th width="25">排序</th>
                    <th width="80">ID</th>
                    <th>分类名称</th>
                    <th width="70">操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if(is_array($Category)): foreach($Category as $key=>$val): if($val['parent'] == 0): ?><tr class="text-c">
                            <td>
                                <input type="text" value="<?php echo ($val['listorder']); ?>" class="input-text" style="width:50px;text-align:center" name="input" index="<?php echo ($val['id']); ?>">
                            </td>
                            <td><?php echo ($val['id']); ?></td>
                            <td class="text-l">
                                <span style="float:left"><?php echo ($val['title']); ?></span>
                                <?php if($val[display] == 0): ?><img src="/thinkphp/Public/admin/img/gear_disable.png" style="float:left;margin:2px 0 0 2px" title="不显示"><?php endif; ?>
                            </td>
                            <td class="f-14">
                                <a title="添加下级栏目" href="<?php echo U('Admin/Category/addchild/',array('id'=>$val['id']));?>" style="margin:0 6px 0 12px"><i class="icon-plus"></i></a>
                                <a title="编辑" href="<?php echo U('Admin/Category/edit/'.$val['id']);?>"><i class="icon-edit"></i></a>
                                <a title="删除" href="javascript:;" class="ml-5 del" index="<?php echo ($val['id']); ?>"><i class="icon-trash"></i></a>
                            </td>
                        </tr>
                        <?php if($val['children'] != ''): $child=explode(',',$val['children']); ?>
                            <?php if(is_array($child)): foreach($child as $k=>$value): if(is_array($Category)): foreach($Category as $key=>$val): if($val['id'] == $value): ?><tr class="text-c tr<?php echo ($val['parent']); ?>">
                                            <td>
                                                <input type="text" value="<?php echo ($val['listorder']); ?>" class="input-text" style="width:50px;text-align:center" name="input" index="<?php echo ($val['id']); ?>">
                                            </td>
                                            <td><?php echo ($val['id']); ?></td>
                                            <td class="text-l">
                                                <?php if(count($child) == $k+1): ?><span class='menu-zizi'></span>
                                                <?php else: ?>
                                                    <span class='menu-zi'></span><?php endif; ?>
                                                <span style="float:left;"><?php echo ($val['title']); ?></span>
                                                <?php if($val[display] == 0): ?><img src="/thinkphp/Public/admin/img/gear_disable.png" style="float:left;margin:2px 0 0 2px" title="不显示"><?php endif; ?>
                                            </td>
                                            <td class="f-14">
                                                <a title="添加下级栏目" href="<?php echo U('Admin/Category/addsan/',array('id'=>$val['id']));?>" style="margin:0 6px 0 12px"><i class="icon-plus"></i></a>
                                                <a title="编辑" href="<?php echo U('Admin/Category/edit/'.$val['id']);?>"><i class="icon-edit"></i></a>
                                                <a title="删除" href="javascript:;" class="ml-5 del2" index="<?php echo ($val['id']); ?>"><i class="icon-trash"></i></a>
                                            </td>
                                        </tr>

                                        <?php if($val['children'] != ''): $child2=explode(',',$val['children']); ?>
                                            <?php foreach($child2 as $k=>$val2){ ?>
                                                <?php foreach($Category as $vals){ ?>
                                                    <?php if($vals['id'] == $val2): ?><tr class="text-c tr<?php echo ($val['parent']); ?>">
                                                            <td>
                                                                <input type="text" value="<?php echo ($vals['listorder']); ?>" class="input-text" style="width:50px;text-align:center" name="input" index="<?php echo ($vals['id']); ?>">
                                                            </td>
                                                            <td><?php echo ($vals['id']); ?></td>
                                                            <td class="text-l" style="padding-left:65px">
                                                                <?php if(count($child2) == $k+1): ?><span class='menu-zizi'></span>
                                                                <?php else: ?>
                                                                    <span class='menu-zi'></span><?php endif; ?>
                                                                <span style="float:left;"><?php echo ($vals['title']); ?></span>
                                                                <?php if($vals[display] == 0): ?><img src="/thinkphp/Public/admin/img/gear_disable.png" style="float:left;margin:2px 0 0 2px" title="不显示"><?php endif; ?>
                                                            </td>
                                                            <td class="f-14">
                                                                <a title="编辑" href="<?php echo U('Admin/Category/edit/'.$vals['id']);?>" style="margin-left:29px"><i class="icon-edit"></i></a>
                                                                <a title="删除" href="javascript:;" class="ml-5 del3" index="<?php echo ($vals['id']); ?>"><i class="icon-trash"></i></a>
                                                            </td>
                                                        </tr><?php endif; ?>
                                            <?php }} endif; endif; endforeach; endif; endforeach; endif; endif; endif; endforeach; endif; ?>
                <?php if(!empty($Category)): ?><tr>
                        <th class="text-r" style="border:none">
                            <button name="system-base-save" id="queding" class="btn btn-success radius" type="submit"><i class="icon-ok"></i> 确定</button>
                        </th>
                    </tr><?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/category_index.js"></script>
<script>
    $('#menu-list').attr('class','selected');
    $('#menu-list li a').eq(0).attr('class','on')
</script>
    </section>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>