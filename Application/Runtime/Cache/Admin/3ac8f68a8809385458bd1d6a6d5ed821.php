<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/thinkphp/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/thinkphp/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/dist/css/wangEditor.min.css" />
<script type="text/javascript" src="/thinkphp/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/wangEditor/dist/js/wangEditor.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="/thinkphp" id="url">
<?php
 $ru=$_SERVER['REQUEST_URI']; $fru=explode('/',$ru); $ye=array_pop($fru); echo '<input type="hidden" value='.$ye.' id="now_ye">'; ?>

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($web['title']); ?></a>
	<span class="Hui-userbox">
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
						<li><a href="<?php echo U('System/img');?>">图片水印</a></li>
						<li><a href="<?php echo U('System/database');?>">数据库</a></li>
						<!-- <li><a href="<?php echo U('System/email');?>">邮箱设置</a></li> -->
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<style>
    .mt-20{margin-top:0}
</style>
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统设置 <span class="c-gray en">&gt;</span> 备份记录
</nav>
<div class="pd-20 text-c">
    <div class="article-class-list cl mt-20">
        <ul class="tab">
            <li><a href="<?php echo U('Admin/System/database');?>">数据表</a></li>
            <li><a href="<?php echo U('Admin/System/dbhistory');?>" class='selected'>备份记录</a></li>
        </ul>
        <table class="table table-border table-bordered table-hover table-bg">
            <thead>
                <tr class="text-c">
                    <th>ID</th>
                    <th>文件名</th>
                    <th>时间</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if(is_array($arr)): foreach($arr as $key=>$val): ?><tr class="text-c">
                        <td><?php echo ($val['id']); ?></td>
                        <td><?php echo ($val['file']); ?></td>
                        <td><?php echo (date('Y-m-d H:i:s',$val['time'])); ?></td>
                        <td class="f-14">
                            <a title="下载" href="<?php echo U('System/download',array('id'=>$val['id']));?>"><i class="icon-download"></i></a>&nbsp;&nbsp;
                            <a title="删除" href="javascript:;" class="del" index="<?php echo ($val["id"]); ?>"><i class="icon-trash"></i></a>
                        </td>
                    </tr><?php endforeach; endif; ?>
            </tbody>
        </table>
        <div id="pageNav" class='pageNav'><?php echo ($page); ?></div>
    </div>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/system_dbhistory.js"></script>
<script>
    $('#menu-cog').attr('class','selected');
    $('#menu-cog li a').eq(3).attr('class','on')
</script>
    </section>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>