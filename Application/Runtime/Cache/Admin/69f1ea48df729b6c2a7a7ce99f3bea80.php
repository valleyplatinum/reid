<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="/Public/dist/css/wangEditor.min.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/Pcsite.css" />
<script type="text/javascript" src="/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/Public/wangEditor/dist/js/wangEditor.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="" id="url">
<input type="hidden" value="" id="app">
<input type="hidden" value="<?php echo CONTROLLER_NAME ?>" class="con">
<input type="hidden" value="<?php echo ACTION_NAME ?>" class="mod">
<input type="hidden" value="<?php echo session('admin_lang') ?>" class="admin_lang">

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($Web['title']); ?></a>
	<span class="Hui-userbox">
		<?php if(session('admin_lang')=='cn'){ ?>
		<span class="c-white">当前站点为中文，切换至</span><a class="btn btn-primary radius ml-10 lang" href="javascript:;" title="切换"><i class="icon-signout"></i> English</a>&nbsp;&nbsp;
		<?php }else{ ?>
		<span class="c-white">当前站点为英文，切换至</span><a class="btn btn-primary radius ml-10 lang" href="javascript:;" title="切换"><i class="icon-signout"></i> 中文</a>&nbsp;&nbsp;
		<?php } ?>
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list2">
				<dt><i class="icon-home"></i> 首页设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Banner/index');?>">Banner</a></li>
						<li><a href="<?php echo U('Qual/index');?>">资质荣誉</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list3">
				<dt><i class="icon-sitemap"></i> 关于我们<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('About/index');?>">关于我们</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list4">
				<dt><i class="icon-leaf"></i> 产品中心<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Product/index');?>">产品中心</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list5">
				<dt><i class="icon-fire"></i> 技术实力<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Tech/index');?>">技术实力</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list6">
				<dt><i class="icon-tint"></i> 应用领域<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Field/index');?>">应用领域</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list7">
				<dt><i class="icon-comments"></i> 公司媒体<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('News/index');?>">公司媒体</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list8">
				<dt><i class="icon-phone"></i> 联系我们<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 应用领域 <span class="c-gray en">&gt;</span> 应用领域
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="<?php echo U('Field/index');?>" title="后退" ><i class="icon-arrow-left"></i></a>
</nav>
<div class="pd-20 text-c" style="padding-top:0px">
    <span class="l"><a href="<?php echo U('Field/add');?>" class="btn btn-primary radius" style="margin-bottom:10px"><i class="icon-plus"></i> 添加应用</a></span>
    <div class="article-class-list cl mt-20">
        <table class="table table-border table-bordered table-hover table-bg">
            <thead>
                <tr class="text-c">
                    <th>栏目</th>
                    <th>标题</th>
                    <th>图片</th>
                    <th width="60">操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if(is_array($arr)): foreach($arr as $key=>$val): ?><tr class="text-c">
                        <td>
                            <?php  switch ($val['type']) { case '1':echo '石油石化行业';break; case '2':echo '锅炉行业';break; case '3':echo '石油炼化工程';break; case '4':echo '工程机械';break; case '5':echo '电力及输送工程';break; case '6':echo '矿山冶金';break; case '7':echo '核电工程';break; case '8':echo '造船工程';break; } ?>
                        </td>
                        <td><?php echo ($val["title"]); ?></td>
                        <td><img src="/Uploads/img<?php echo ($val["img"]); ?>" alt="" style="width:100px"></td>
                        <td class="f-14">
                            <a title="编辑" href="<?php echo U('Field/edit',array('id'=>$val[id]));?>"><i class="icon-edit"></i></a>&nbsp;&nbsp;
                            <a title="删除" href="javascript:;" class="del" index="<?php echo ($val['id']); ?>"><i class="icon-trash"></i></a>
                        </td>
                    </tr><?php endforeach; endif; ?>
            </tbody>
        </table>
    </div>
</div>
<script src='/Public/admin/js/field_index.js'></script>
<script>
    $('#menu-list6').attr('class','selected');
    $('#menu-list6 li a').eq(0).attr('class','on')
</script>
    </section>
</div>
<script type="text/javascript" src="/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>