<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/thinkphp/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/thinkphp/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/dist/css/wangEditor.min.css" />
<script type="text/javascript" src="/thinkphp/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/wangEditor/dist/js/wangEditor.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="/thinkphp" id="url">
<?php
 $ru=$_SERVER['REQUEST_URI']; $fru=explode('/',$ru); $ye=array_pop($fru); echo '<input type="hidden" value='.$ye.' id="now_ye">'; ?>

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($Web['title']); ?></a>
	<span class="Hui-userbox">
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
						<li><a href="<?php echo U('System/img');?>">图片水印</a></li>
						<!-- <li><a href="<?php echo U('System/database');?>">数据库</a></li> -->
						<!-- <li><a href="<?php echo U('System/email');?>">邮箱设置</a></li> -->
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<style>
    .file{position: relative;display: inline-block;background: #D0EEFF;border: 1px solid #99D3F5;border-radius: 4px;padding:0 5px 0 5px;overflow: hidden;color: #1E88C7;text-decoration: none;
        text-indent: 0;line-height: 20px;float:left; font-size:12px;}
    .file input {position: absolute;font-size: 100px;right: 0;top: 0;opacity: 0;}
    .file:hover {background: #AADFFD;border-color: #78C3F3;color: #004974;text-decoration: none;}
    .xin{font-size:12px;float:left;margin:0 0 0 10px;color:green;cursor:pointer;}
</style>
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统设置 <span class="c-gray en">&gt;</span> 图片水印
</nav>
<div class="pd-20">
    <table class="table table-border table-bordered table-hover table-bg">
        <tr>
            <th class="text-r" width="100">水印</th>
            <td>
                <div style="width:80px;float:left"><label id="guanbi"><input type="radio" value="0" name="open">关闭</label></div>
                <div style="width:80px;float:left"><label id="kaiqi"><input type="radio" value="1" name="open">开启</label></div>
                <input type="hidden" value="<?php echo ($arr["open"]); ?>" id="open">
            </td>
        </tr>
        <tr class="kai">
            <th class="text-r" width="100">样式</th>
            <td>
                <div style="width:80px;float:left"><label id="tu"><input type="radio" value="1" name="style">图片式</label></div>
                <div style="width:80px;float:left"><label id="wen"><input type="radio" value="2" name="style">文字式</label></div>
                <input type="hidden" value="<?php echo ($arr["style"]); ?>" id="style">
            </td>
        </tr>
        <tr class="kai tu">
            <th class="text-r" width="100">图片</th>
            <td>
                <span class="golo">
                    <?php if($arr["img"] != ''): ?><img src="/thinkphp/Uploads/water/<?php echo ($arr["img"]); ?>" alt="" style='float:left;margin:3px 5px 0 0' class='img1'>
                        <input type="hidden" value="0" class="shi">
                    <?php else: ?>
                        <input type="hidden" value="0" class="shi"><?php endif; ?>
                </span>
                <a href="javascript:;" class="file">选择图片
                    <input type="file" style="" onchange="file()" name="img" id="img">
                </a>
            </td>
        </tr>
        <tr class="kai tou">
            <th class="text-r" width="100">透明度</th>
            <td>
                <input type="text" id="opacity" value="<?php echo ($arr["opacity"]); ?>" style="width:50px" class="input-text" maxlength="3" onkeyup="value=this.value.replace(/\D+/g,'')">
                请设置为0-100之间的数字，0代表完全透明，100代表不透明
            </td>
        </tr>
        <tr class="kai wen">
            <th class="text-r" width="100">文字</th>
            <td><input type="text" id="word" value="<?php echo trim($arr['word']) ?>" style="width:500px" class="input-text"></td>
        </tr>
        <tr class="kai">
            <th class="text-r" width="100">水印位置</th>
            <td>
                <select id="loca" class="select">
                    <option value="1">顶部居左</option>
                    <option value="2">顶部居中</option>
                    <option value="3">顶部居右</option>
                    <option value="4">中部居左</option>
                    <option value="5">中部居中</option>
                    <option value="6">中部居右</option>
                    <option value="7">底部居左</option>
                    <option value="8">底部居中</option>
                    <option value="9">底部居右</option>
                </select>
                <input type="hidden" value="<?php echo ($arr["loca"]); ?>" class="loca">
            </td>
        </tr>
        <tr>
            <th class="text-r"></th>
            <td><button name="system-base-save" id="queding" class="btn btn-success radius" type="submit"><i class="icon-ok"></i> 确定</button></td>
        </tr>
    </tabel>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/system_img.js"></script>
<script>
    var admin_tou=$('#admin_tou').val();
    var urls=$('#url').val();

    function file(){
        if ($("#img").val().length > 0) {
            ajaxFileUpload();
        }
        else {
            alert("请选择图片");
        }
    }
    function ajaxFileUpload() {
        $.ajaxFileUpload
        (
            {
                url: urls+'/Admin/Bianjiqi/water', //用于文件上传的服务器端请求地址
                secureuri: false, //一般设置为false
                fileElementId: 'img', //文件上传空间的id属性  <input type="file" id="file" name="file" />
                dataType: 'HTML', //返回值类型 一般设置为json
                success: function (data, status)  //服务器成功响应处理函数
                {
                    if(data=='no'){
                        var d = dialog({
                            title: admin_tou,
                            content: '上传失败',
                            cancel: false,
                            okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
                            ok: function () {}
                        });
                        d.showModal();
                        return false;
                    }
                    $('.img1').remove();
                    var zhi="<img src="+urls+"/Uploads/water/"+data+" style='float:left;margin:3px 5px 0 0' class='img1'><input type='hidden' value='1' class='shi'>";
                    $('.golo').html(zhi);

                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            alert(data.msg);
                        }
                    }
                },
                error: function (data, status, e)//服务器响应失败处理函数
                {
                    alert(e);
                }
            }
        )
        return false;
    }
</script>
<script>
    $('#menu-cog').attr('class','selected');
    $('#menu-cog li a').eq(2).attr('class','on')
</script>
    </section>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>