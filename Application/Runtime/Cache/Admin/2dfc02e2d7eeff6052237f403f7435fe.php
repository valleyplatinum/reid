<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="/Public/dist/css/wangEditor.min.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/Pcsite.css" />
<script type="text/javascript" src="/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/Public/wangEditor/dist/js/wangEditor.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="" id="url">
<input type="hidden" value="" id="app">

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($Web['title']); ?></a>
	<span class="Hui-userbox">
		<?php if(session('admin_lang')=='cn'){ ?>
		<a class="btn btn-primary radius ml-10 lang" href="javascript:;" title="切换"><i class="icon-signout"></i> English</a>&nbsp;&nbsp;
		<?php }else{ ?>
		<a class="btn btn-primary radius ml-10 lang" href="javascript:;" title="切换"><i class="icon-signout"></i> 中文</a>&nbsp;&nbsp;
		<?php } ?>
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list2">
				<dt><i class="icon-home"></i> 首页设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Banner/index');?>">Banner</a></li>
						<li><a href="<?php echo U('Qual/index');?>">资质荣誉</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list3">
				<dt><i class="icon-sitemap"></i> 关于我们<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('About/index');?>">关于我们</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list4">
				<dt><i class="icon-leaf"></i> 产品中心<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Product/index');?>">产品中心</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list5">
				<dt><i class="icon-fire"></i> 技术实力<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Tech/index');?>">技术实力</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list6">
				<dt><i class="icon-tint"></i> 应用领域<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Field/index');?>">应用领域</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list7">
				<dt><i class="icon-comments"></i> 公司媒体<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('News/index');?>">公司媒体</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list8">
				<dt><i class="icon-phone"></i> 联系我们<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 公司媒体 <span class="c-gray en">&gt;</span> 公司媒体 <span class="c-gray en">&gt;</span> 添加媒体
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:;history.go(-1)" title="后退" ><i class="icon-arrow-left"></i></a>
</nav>
<div class="pd-20">
    <table class="table table-border table-bordered table-hover table-bg">
        <tr>
            <th class="text-r" width="100">标题</th>
            <td><input type="text" id="title" value="" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">摘要</th>
            <td><textarea id="description" style="max-width:508px;min-width:508px;height:100px"></textarea></td>
        </tr>
        <tr>
            <th class="text-r" width="100">内容</th>
            <td><div style="width:1200px"><textarea id="content" style="min-height:600px;" name="text"></textarea></div></td>
        </tr>
        
        <tr>
            <th class="text-r"></th>
            <td><button name="system-base-save" id="queding" class="btn btn-success radius" type="submit"><i class="icon-ok"></i> 确定</button></td>
        </tr>
    </tabel>
</div>
<script src='/Public/admin/js/news_add.js'></script>
<script>
    $('#menu-list7').attr('class','selected');
    $('#menu-list7 li a').eq(0).attr('class','on')
</script>
    </section>
</div>
<script type="text/javascript" src="/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>