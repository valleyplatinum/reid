<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/thinkphp/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/thinkphp/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/dist/css/wangEditor.min.css" />
<script type="text/javascript" src="/thinkphp/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/wangEditor/dist/js/wangEditor.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="/thinkphp" id="url">
<?php
 $ru=$_SERVER['REQUEST_URI']; $fru=explode('/',$ru); $ye=array_pop($fru); echo '<input type="hidden" value='.$ye.' id="now_ye">'; ?>

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($web['title']); ?></a>
	<span class="Hui-userbox">
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
						<li><a href="<?php echo U('System/img');?>">图片水印</a></li>
						<!-- <li><a href="<?php echo U('System/database');?>">数据库</a></li> -->
						<!-- <li><a href="<?php echo U('System/email');?>">邮箱设置</a></li> -->
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
	<div class="pd-20" style="padding-top:20px;">
		<p class="f-20 text-success">欢迎登录<?php echo ($web['title']); ?>后台管理中心</p>
		<p>登录次数：<?php echo ($arr['log_num']); ?></p>
		<p>本次登录IP：<?php echo ($arr['log_ip']); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本次登录时间：<?php echo date('Y-m-d H:i',$arr['log_time']) ?></p>

		<table class="table table-border table-bordered table-bg mt-20">
	  		<thead>
    			<tr>
	      			<th colspan="2" scope="col">服务器信息</th>
	    		</tr>
	  		</thead>
	  		<tbody>
	    		<tr>
	      			<th width="200">PHP 版本</th>
	      			<td><?php echo PHP_VERSION; ?></td>
	    		</tr>
	    		<tr>
	      			<td>MySQL 版本</td>
	      			<td><?php echo mysql_get_server_info(); ?></td>
	    		</tr>
	    		<tr>
	      			<td>服务器操作系统</td>
	      			<td><?PHP echo php_uname(); ?></td>
	    		</tr>
	    		<tr>
	      			<td>文件上传限制</td>
	      			<td><?PHP echo get_cfg_var ("upload_max_filesize")? get_cfg_var ("upload_max_filesize"):"不允许上传附件"; ?></td>
    			</tr>
	    		<tr>
	      			<td>数据库字符集</td>
	      			<td>UTF-8 Unicode (utf8)</td>
	    		</tr>
	    		<tr>
	      			<td>Web 服务器</td>
	      			<td><?php echo $_SERVER['SERVER_SOFTWARE'] ?></td>
	    		</tr>
	  		</tbody>
		</table>
	</div>
    </section>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>