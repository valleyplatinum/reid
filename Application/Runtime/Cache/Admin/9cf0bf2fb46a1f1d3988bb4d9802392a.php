<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/thinkphp/favicon.ico" >
<LINK rel="Shortcut Icon" href="/thinkphp/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/ui-dialog.css" />
<script type="text/javascript" src="/thinkphp/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/thinkphp/Public/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="/thinkphp" id="url">

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>">ThinkPHP</a>
	<span class="Hui-userbox">
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 导航设置 <span class="c-gray en">&gt;</span> 导航设置
</nav>
<div class="pd-20 text-c">
    上级栏目：
    <select class="select" id="catid">
        <option value="0">一级栏目</option>
        <?php if(is_array($arr)): foreach($arr as $key=>$val): if($val['parent'] == 0): ?><option value="<?php echo ($val['id']); ?>"><?php echo ($val['title']); ?></option>
            <?php else: endif; endforeach; endif; ?>
    </select>
    <input class="input-text" style="width:250px;margin-right:3px" type="text" id="title">
    <button type="button" class="btn btn-success" id="add"><i class="icon-plus"></i> 添加</button>

    <div class="article-class-list cl mt-20">
        <table class="table table-border table-bordered table-hover table-bg">
            <thead>
                <tr class="text-c">
                    <th>ID</th>
                    <th>User</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if(is_array($arr)): foreach($arr as $key=>$val): ?><tr class="text-c">
                        <td><?php echo ($val['id']); ?></td>
                        <td><?php echo ($val['user']); ?></td>
                        <td class="f-14">
                            <a title="删除" href="javascript:;" class="ml-5 del" index="<?php echo ($val['id']); ?>"><i class="icon-trash"></i></a>
                        </td>
                    </tr><?php endforeach; endif; ?>
            </tbody>
        </table>

        <div id="pageNav" class="pageNav"><?php echo ($page); ?></div>
    </div>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/user_index.js"></script>

    </section>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>