<?php if (!defined('THINK_PATH')) exit();?>﻿<!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/thinkphp/favicon.ico" >
<LINK rel="Shortcut Icon" href="/thinkphp/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.login.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/ui-dialog.css" />

<script type="text/javascript" src="/thinkphp/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/dialog-min.js"></script>
</head>
<body>
<input type="hidden" value="/thinkphp" id="url">
<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<div class="header"></div>
<div class="loginWraper">
    <div id="loginform" class="loginBox">

        <div class="formRow user">
            <input id="username" name="" type="text" placeholder="账户" class="input_text input-big">
        </div>
        <div class="formRow password">
            <input id="password" name="" type="password" placeholder="密码" class="input_text input-big">
        </div>
        <div class="formRow yzm">
            <input class="input_text input-big" type="text" placeholder="验证码" style="width:170px;margin-right:5px" id="sms">
            <img src="verify" alt="" id="verify_img" style="height:40px;width:180px;cursor:pointer"></div>
        <div class="formRow button">
            <input name="" type="submit" class="btn radius btn-success btn-big" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;" id="login">
            <input name="" type="reset" class="btn radius btn-default btn-big" value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;" id="reset">
        </div>

    </div>
</div>
<div class="footer"><?php echo ($web['banquan']); ?>&nbsp;&nbsp;&nbsp;<?php echo ($web['icp']); ?></div>
<script>
    

    // $('button').click(function(){
    //     $.post('check_verify',{code:$("#j_verify").val()},function(data){
    //         alert(data)
    //     })
    // })
</script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/login.js"></script>
</body>
</html>