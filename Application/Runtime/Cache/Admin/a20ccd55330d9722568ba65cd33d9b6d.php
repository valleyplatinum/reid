<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/thinkphp/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/thinkphp/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/dist/css/wangEditor.min.css" />
<script type="text/javascript" src="/thinkphp/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/wangEditor/dist/js/wangEditor.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="/thinkphp" id="url">
<?php
 $ru=$_SERVER['REQUEST_URI']; $fru=explode('/',$ru); $ye=array_pop($fru); echo '<input type="hidden" value='.$ye.' id="now_ye">'; ?>

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($web['title']); ?></a>
	<span class="Hui-userbox">
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
						<!-- <li><a href="<?php echo U('System/email');?>">邮箱设置</a></li> -->
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 系统设置 <span class="c-gray en">&gt;</span> 邮箱设置
</nav>
<div class="pd-20">
    <table class="table table-border table-bordered table-hover table-bg">
        <tr>
            <th class="text-r" width="100">邮件发送模式</th>
            <td><input type="radio" checked> SMTP函数发送</td>
        </tr>
        <tr>
            <th class="text-r" width="100">邮件服务器</th>
            <td><input type="text" id="fuwuqi" value="<?php echo ($arr["fuwuqi"]); ?>" style="width:500px" class="input-text" maxlength="20"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">邮件发送端口</th>
            <td><input type="text" id="duankou" value="<?php echo ($arr["duankou"]); ?>" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">发件人地址</th>
            <td><input type="text" id="fajianren" value="<?php echo ($arr["fajianren"]); ?>" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">验证用户名</th>
            <td><input type="text" id="you" value="<?php echo ($arr["you"]); ?>" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">验证密码</th>
            <td><input type="text" id="password" value="<?php echo ($arr["password"]); ?>" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r"></th>
            <td>
                <button name="system-base-save" id="queding" class="btn btn-success radius" type="submit"><i class="icon-ok"></i> 确定</button>
                <button name="system-base-save" id="ceshi" class="btn btn-primary radius" type="submit">测试发送</button>
            </td>
        </tr>
    </tabel>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/system_email.js"></script>
<script>
    $('#menu-cog').attr('class','selected');
    $('#menu-cog li a').eq(2).attr('class','on')
</script>
    </section>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>