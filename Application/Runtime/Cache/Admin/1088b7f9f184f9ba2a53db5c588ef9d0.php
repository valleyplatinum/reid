<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/thinkphp/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/thinkphp/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/thinkphp/Public/admin/css/ui-dialog.css" />
<script type="text/javascript" src="/thinkphp/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/thinkphp/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/thinkphp/Public/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="/thinkphp" id="url">
<?php
 $ru=$_SERVER['REQUEST_URI']; $fru=explode('/',$ru); $ye=array_pop($fru); echo '<input type="hidden" value='.$ye.' id="now_ye">'; ?>

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($web['title']); ?></a>
	<span class="Hui-userbox">
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 导航设置 <span class="c-gray en">&gt;</span> 导航设置 <span class="c-gray en">&gt;</span> 添加二级导航
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:;history.go(-1)" title="后退" ><i class="icon-arrow-left"></i></a>
</nav>
<div class="pd-20">
    <table class="table table-border table-bordered table-hover table-bg">
        <tr>
            <th class="text-r" width="100">上级栏目</th>
            <td><?php echo ($arr["title"]); ?></td>
        </tr>
        <tr>
            <th class="text-r" width="100">标题</th>
            <td><input type="text" id="title" value="" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">对否显示</th>
            <td>
                <select id="display" class="select">
                    <option value="1">显示</option>
                    <option value="0">不显示</option>
                </select>
            </td>
        </tr>
        <tr>
            <th class="text-r" width="100">PC链接</th>
            <td><input type="text" id="pcurl" value="" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">Mobile链接</th>
            <td><input type="text" id="wap_url" value="" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">网页标题</th>
            <td><input type="text" id="web_title" value="" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">网页关键字</th>
            <td><input type="text" id="web_description" value="" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r" width="100">网页描述</th>
            <td><input type="text" id="web_text" value="" style="width:500px" class="input-text"></td>
        </tr>
        <tr>
            <th class="text-r"></th>
            <td><button name="system-base-save" id="queding" class="btn btn-success radius" type="submit"><i class="icon-ok"></i> 确定</button></td>
        </tr>
    </tabel>
</div>
<input type="hidden" value="<?php echo I('get.id') ?>" class="parent">
<script type="text/javascript" src="/thinkphp/Public/admin/js/category_addchild.js"></script>
<script>
    $('#menu-list').attr('class','selected');
    $('#menu-list li a').eq(0).attr('class','on')
</script>
    </section>
</div>
<script type="text/javascript" src="/thinkphp/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/thinkphp/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>