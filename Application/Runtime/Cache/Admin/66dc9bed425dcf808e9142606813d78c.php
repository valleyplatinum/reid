<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html style="overflow-y:hidden;">
<head>
<title><?php echo ($web['title']); ?></title>
<meta name="keywords" content="<?php echo ($web['keywords']); ?>">
<meta name="description" content="<?php echo ($web['description']); ?>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/Public/favicon.ico" >
<LINK rel="Shortcut Icon" href="/Public/favicon.ico" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/H-ui.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/ui-dialog.css" />
<link rel="stylesheet" type="text/css" href="/Public/dist/css/wangEditor.min.css" />
<link rel="stylesheet" type="text/css" href="/Public/admin/css/Pcsite.css" />
<script type="text/javascript" src="/Public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/Public/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/Public/js/dialog-min.js"></script>
<script type="text/javascript" src="/Public/wangEditor/dist/js/wangEditor.js"></script>
</head>
<body>

<input type="hidden" value="<?php echo $admin_tou ?>" id="admin_tou">
<input type="hidden" value="" id="url">
<input type="hidden" value="" id="app">
<input type="hidden" value="<?php echo CONTROLLER_NAME ?>" class="con">
<input type="hidden" value="<?php echo ACTION_NAME ?>" class="mod">
<input type="hidden" value="<?php echo session('admin_lang') ?>" class="admin_lang">

<header class="Hui-header cl">
	<a class="Hui-logo l" href="<?php echo U('Admin/Index/index');?>"><?php echo ($Web['title']); ?></a>
	<span class="Hui-userbox">
		<?php if(session('admin_lang')=='cn'){ ?>
		<span class="c-white">当前站点为中文，切换至</span><a class="btn btn-primary radius ml-10 lang" href="javascript:;" title="切换"><i class="icon-signout"></i> English</a>&nbsp;&nbsp;
		<?php }else{ ?>
		<span class="c-white">当前站点为英文，切换至</span><a class="btn btn-primary radius ml-10 lang" href="javascript:;" title="切换"><i class="icon-signout"></i> 中文</a>&nbsp;&nbsp;
		<?php } ?>
		<span class="c-white">超级管理员：admin</span>
		<a class="btn btn-danger radius ml-10" href="<?php echo U('Admin/logout');?>" title="退出"><i class="icon-off"></i> 退出</a>
	</span>
</header>
<div class="cl Hui-main">
	<aside class="Hui-aside" style="">
		<div class="menu_dropdown bk_2">
			<dl id="menu-cog">
				<dt><i class="icon-cog"></i> 系统设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('System/index');?>">站点设置</a></li>
						<li><a href="<?php echo U('System/password');?>">修改密码</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list">
				<dt><i class="icon-list"></i> 导航设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Category/index');?>">导航设置</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list2">
				<dt><i class="icon-home"></i> 首页设置<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Banner/index');?>">Banner</a></li>
						<li><a href="<?php echo U('Qual/index');?>">资质荣誉</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list3">
				<dt><i class="icon-sitemap"></i> 关于我们<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('About/index');?>">关于我们</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list4">
				<dt><i class="icon-leaf"></i> 产品中心<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Product/index');?>">产品中心</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list5">
				<dt><i class="icon-fire"></i> 技术实力<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Tech/index');?>">技术实力</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list6">
				<dt><i class="icon-tint"></i> 应用领域<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Field/index');?>">应用领域</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list7">
				<dt><i class="icon-comments"></i> 公司媒体<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('News/index');?>">公司媒体</a></li>
					</ul>
				</dd>
			</dl>
			<dl id="menu-list8">
				<dt><i class="icon-phone"></i> 联系我们<b></b></dt>
				<dd>
					<ul>
						<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	</aside>

	<section class="Hui-article">
<nav class="Hui-breadcrumb">
    <i class="icon-home"></i> 首页 <span class="c-gray en">&gt;</span> 关于我们 <span class="c-gray en">&gt;</span> 关于我们
</nav>
<div class="pd-20">
    <table class="table table-border table-bordered table-hover table-bg">
        <tr>
            <th class="text-r" width="100">首页</th>
            <td><textarea id="index" style="max-width:508px;min-width:508px;height:200px"><?php echo ($arr["index"]); ?></textarea></td>
        </tr>
        <tr>
            <th class="text-r" width="100">首页图片</th>
            <td>
                <span class="golo">
                    <img src="/Uploads/img<?php echo ($arr["img"]); ?>" class='img' style='float:left;margin:3px 5px 0 0;width:300px;'>
                </span>
                <a href="javascript:;" class="file">选择图片
                    <input type="file" style="" onchange="file()" name="img" id="img">
                    <input type="hidden" value="<?php echo ($arr["img"]); ?>" id="iimg1">
                </a>
            </td>
        </tr>
        <tr>
            <th class="text-r" width="100">简介左</th>
            <td><textarea id="left" style="max-width:508px;min-width:508px;height:200px"><?php echo ($arr["left"]); ?></textarea></td>
        </tr>
        <tr>
            <th class="text-r" width="100">简介右</th>
            <td><textarea id="right" style="max-width:508px;min-width:508px;height:200px"><?php echo ($arr["right"]); ?></textarea></td>
        </tr>
        <tr>
            <th class="text-r" width="100">团队建设</th>
            <td>
                <a href="javascript:;" class="file">选择图片
                    <input type="file" style="" onchange="file1()" name="img1" id="img1">
                </a>
                <div style="clear:both"></div>
                <span class="golo1">
                    <?php if($arr["img1"] != ''): $img1=explode(',',$arr['img1']); ?>
                        <?php if(is_array($img1)): foreach($img1 as $key=>$val): ?><div class="baotu" id='bao<?php echo ($key); ?>'>
                                <img src="<?php echo ($val); ?>">
                                <div class='shanchu' index="<?php echo ($val); ?>" ids="1" k="<?php echo ($key); ?>" type="img1">删除</div>
                            </div><?php endforeach; endif; endif; ?>
                </span>
            </td>
        </tr>
        <tr>
            <th class="text-r" width="100">厂区厂貌</th>
            <td>
                <a href="javascript:;" class="file">选择图片
                    <input type="file" style="" onchange="file2()" name="img2" id="img2">
                </a>
                <div style="clear:both"></div>
                <span class="golo2">
                    <?php if($arr["img2"] != ''): $img2=explode(',',$arr['img2']); ?>
                        <?php if(is_array($img2)): foreach($img2 as $key=>$val): ?><div class="baotu" id='bao<?php echo ($key); ?>'>
                                <img src="<?php echo ($val); ?>">
                                <div class='shanchu' index="<?php echo ($val); ?>" ids="1" k="<?php echo ($key); ?>" type="img2">删除</div>
                            </div><?php endforeach; endif; endif; ?>
                </span>
            </td>
        </tr>
        
        <tr>
            <th class="text-r"></th>
            <td><button name="system-base-save" id="queding" class="btn btn-success radius" type="submit"><i class="icon-ok"></i> 确定</button></td>
        </tr>
    </tabel>
</div>
<script src='/Public/admin/js/about_index.js'></script>
<script>
    var admin_tou=$('#admin_tou').val();
    var urls=$('#app').val();

    function file(){
        if ($("#img").val().length > 0) {
            ajaxFileUpload();
        }
        else {
            alert("请选择图片");
        }
    }
    function ajaxFileUpload() {
        $.ajaxFileUpload
        (
            {
                url: urls+'/Admin/Bianjiqi/img', //用于文件上传的服务器端请求地址
                secureuri: false, //一般设置为false
                fileElementId: 'img', //文件上传空间的id属性  <input type="file" id="file" name="file" />
                dataType: 'HTML', //返回值类型 一般设置为json
                success: function (data, status)  //服务器成功响应处理函数
                {
                    if(data=='no'){
                        var d = dialog({
                            title: admin_tou,
                            content: '上传失败',
                            cancel: false,
                            okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
                            ok: function () {}
                        });
                        d.showModal();
                        return false;
                    }
                    $('.img').remove();
                    var zhi="<img src="+urls+'/Uploads/img'+data+" class='img' style='float:left;margin:3px 5px 0 0;width:300px;'>";
                    $('#iimg1').val(data);
                    $('.golo').html(zhi);

                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            alert(data.msg);
                        }
                    }
                },
                error: function (data, status, e)//服务器响应失败处理函数
                {
                    alert(e);
                }
            }
        )
        return false;
    }

    function file1(){
        if ($("#img1").val().length > 0) {
            ajaxFileUpload1();
        }
        else {
            alert("请选择图片");
        }
    }
    function ajaxFileUpload1() {
        $.ajaxFileUpload
        (
            {
                url: urls+'/Admin/Bianjiqi/img_all2?id=1', //用于文件上传的服务器端请求地址
                secureuri: false, //一般设置为false
                fileElementId: 'img1', //文件上传空间的id属性  <input type="file" id="file" name="file" />
                dataType: 'HTML', //返回值类型 一般设置为json
                success: function (data, status)  //服务器成功响应处理函数
                {
                    if(data=='no'){
                        var d = dialog({
                            title: admin_tou,
                            content: '上传失败',
                            cancel: false,
                            okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
                            ok: function () {}
                        });
                        d.showModal();
                        return false;
                    }
                    $('.golo1').html(data);

                    $('.shanchu').unbind('click').bind('click',function(){
                        var ids=$(this).attr('ids');
                        var addr=$(this).attr('index');
                        var app=$('#app').val();
                        var k=$(this).attr('k');
                        var type=$(this).attr('type');
                        $.post(app+'/Admin/About/del_img',{id:ids,img:addr,type:type},function(data){
                            if(data==1){
                                if(type=='img1'){
                                    $('.golo1 #bao'+k).fadeOut(500);
                                }else{
                                    $('.golo2 #bao'+k).fadeOut(500);
                                }
                            }
                        })
                    })

                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            alert(data.msg);
                        }
                    }
                },
                error: function (data, status, e)//服务器响应失败处理函数
                {
                    alert(e);
                }
            }
        )
        return false;
    }

    function file2(){
        if ($("#img2").val().length > 0) {
            ajaxFileUpload2();
        }
        else {
            alert("请选择图片");
        }
    }
    function ajaxFileUpload2() {
        $.ajaxFileUpload
        (
            {
                url: urls+'/Admin/Bianjiqi/img_all2?id=1', //用于文件上传的服务器端请求地址
                secureuri: false, //一般设置为false
                fileElementId: 'img2', //文件上传空间的id属性  <input type="file" id="file" name="file" />
                dataType: 'HTML', //返回值类型 一般设置为json
                success: function (data, status)  //服务器成功响应处理函数
                {
                    if(data=='no'){
                        var d = dialog({
                            title: admin_tou,
                            content: '上传失败',
                            cancel: false,
                            okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
                            ok: function () {}
                        });
                        d.showModal();
                        return false;
                    }
                    $('.golo2').html(data);

                    $('.shanchu').unbind('click').bind('click',function(){
                        var ids=$(this).attr('ids');
                        var addr=$(this).attr('index');
                        var app=$('#app').val();
                        var k=$(this).attr('k');
                        var type=$(this).attr('type');
                        $.post(app+'/Admin/About/del_img',{id:ids,img:addr,type:type},function(data){
                            if(data==1){
                                if(type=='img1'){
                                    $('.golo1 #bao'+k).fadeOut(500);
                                }else{
                                    $('.golo2 #bao'+k).fadeOut(500);
                                }
                            }
                        })
                    })

                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            alert(data.msg);
                        }
                    }
                },
                error: function (data, status, e)//服务器响应失败处理函数
                {
                    alert(e);
                }
            }
        )
        return false;
    }

    var text= document.getElementById("left").innerHTML;
    for(var i=0;i<=10;i++){
        text=text.replace("&lt;br /&gt;"," ");
    }
    $('#left').val(text);

    var text= document.getElementById("right").innerHTML;
    for(var i=0;i<=10;i++){
        text=text.replace("&lt;br /&gt;"," ");
    }
    $('#right').val(text);
</script>
<script>
    $('#menu-list3').attr('class','selected');
    $('#menu-list3 li a').eq(0).attr('class','on')
</script>
    </section>
</div>
<script type="text/javascript" src="/Public/admin/js/Validform_v5.3.2_min.js"></script>
<script type="text/javascript" src="/Public/admin/js/H-ui.js"></script>
<script type="text/javascript" src="/Public/admin/js/H-ui.admin.js"></script>
</body>
</html>