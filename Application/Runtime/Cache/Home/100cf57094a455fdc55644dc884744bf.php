<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="<?php echo U('Index/index');?>">首页</a></li>
				<li><a href="<?php echo U('About/index');?>">走进瑞德</a></li>
				<li><a href="<?php echo U('Product/index');?>">产品展示</a></li>
				<li><a href="<?php echo U('Tech/index');?>">设备展示</a></li>
				<li><a href="<?php echo U('Quality/index');?>">品质管理</a></li>
				<li><a href="<?php echo U('After/index');?>">售后服务</a></li>
				<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="http://testing.com/">中文</a></li>
						<li><a href="http://en.testing.com/">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/banner1.jpg" alt="" />
			<div class="font font1" >
				<div class="f1"><img src="/Public/index/img/b-font1.png" alt="" /></div>
				<div class="f2">专注法兰锻件制造</div>
				<div class="f3">20 years dedicated to the manufacture of flange forgings</div>
			</div>
		</div>
	</div>
	<!-- Add Pagination -->
	<div class="swiper-pagination"></div>
</div>
<div class="center">
	<div class="catposBox">
		<a href="#">首页</a>
		<em>></em>
		<span>设备展示</span>
	</div>
	<div class="sbCenter">
		<div class="item">
			<div class="img"><img src="/Public/index/img/sb1.jpg" alt="" /></div>
			<div class="font">
				<div class="t">锻造锤</div>
				<p>
					最大重量: 25 Tons<br>
					最大行程: 6000 mm<br>
					台面尺寸 : 4000 mm x 1500 mm<br>
					锻造次数 : 30 / min
				</p>
			</div>
		</div>
		<div class="item">
			<div class="img"><img src="/Public/index/img/sb2.jpg" alt="" /></div>
			<div class="font">
				<div class="t">碾环机</div>
				<p>
					最大直径: 5000mm<br>
					最大高度: 1000 mm<br>
					锥辊 : 1170mm
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb3.jpg" alt="" /></div>
			<div class="font">
				<div class="t">热处理</div>
				<p>
					3000mm*6000mm 台式电阻炉
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb4.jpg" alt="" /></div>
			<div class="font">
				<div class="t">热处理</div>
				<p>
					RJ2 x 3000、RJ2 x 5000、全纤维井式电炉采用PIMS制,控温精度高,炉温均匀性好。
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb5.jpg" alt="" /></div>
			<div class="font">
				<div class="t">数控车床</div>
				<p>
					粗糙度:1.6<br>
					精确度: 0.03mm
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb6.jpg" alt="" /></div>
			<div class="font">
				<div class="t">数控车床</div>
				<p>
					最大直径: 5000 mm<br>
					最大高度: 1200 mm
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb7.jpg" alt="" /></div>
			<div class="font">
				<div class="t">数控高速钻床</div>
				<p>
					最大直径: 3200 mm<br>
					最大厚度: 500mm<br>
					最大钻孔直径 : 105 mm
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb8.jpg" alt="" /></div>
			<div class="font">
				<div class="t">数控高速钻床</div>
				<p>
					最大直径: 5000 mm<br>
					最大厚度: 500mm<br>
					最大钻孔直径 : 105 mm
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb9.jpg" alt="" /></div>
			<div class="font">
				<div class="t">龙门加工中心</div>
				<p>
					最大直径: 3000 mm<br>
					最大厚度: 800mm<br>
					最大重量：15T
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb10.jpg" alt="" /></div>
			<div class="font">
				<div class="t">CNC加工中心</div>
				<p>
					加工精度：0.01<br>
					粗糙度：1.6<br>
					配套：第四轴
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb11.jpg" alt="" /></div>
			<div class="font">
				<div class="t">线切割</div>
				<p>
					最大直径: 1400 mm<br>
					最大厚度: 800mm
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb12.jpg" alt="" /></div>
			<div class="font">
				<div class="t">线切割</div>
				<p>
					最大直径: 1400 mm<br>
					最大厚度: 800mm
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb13.jpg" alt="" /></div>
			<div class="font">
				<div class="t">数控等离子切割</div>
				<p>
					规格：7500mm*18000mm<br>
					切割厚度：6mm-300mm
				</p>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="/Public/index/img/sb14.jpg" alt="" /></div>
			<div class="font">
				<div class="t">深孔镗床</div>
				<p>
					最大长度：2000mm<br>
					最大孔直径：200mm
				</p>
			</div>
		</div>

	</div>

</div>

<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
</script>