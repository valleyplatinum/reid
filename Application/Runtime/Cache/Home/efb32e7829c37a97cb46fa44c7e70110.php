<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="http://testing.com/">首页</a></li>
				<li><a href="http://testing.com/About/index">走进瑞德</a></li>
				<li><a href="http://testing.com/Product/index">产品展示</a></li>
				<li><a href="http://testing.com/Tech/index">设备展示</a></li>
				<li><a href="http://testing.com/">品质管理</a></li>
				<li><a href="http://testing.com/After/index">售后服务</a></li>
				<li><a href="http://testing.com/Contact/index">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="#">中文</a></li>
						<li><a href="#">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/about4.jpg" alt="" />
		</div>
	</div>
</div>
<div class="center">
	<div class="catposBox">
		<a href="#">首页</a>
		<em>></em>
		<span>售后服务</span>
	</div>
	<div class="aboutCenter">
		<div class="shMain">
			<div class="title">公司售后服务机构图</div>
			<img src="/Public/index/img/sh1.jpg" alt="" />
			<div class="line"></div>
			<p>1、公司建立了总经理领导下的售后服务体系，授权质量负责人全面开展工作，质量负责人对售后服务质量负全责，具体领导售后服务工作和质量管理工作。市场部负责组织和协调售后服务人员，认真履售后服务质量职责。我公司售后服务人员12人，均为我公司经验丰富、技术过硬的人员，技术熟练，熟知产品的性能、结构和特点，解决问题能力强。</p>
			<p>2、市场部在每批产品售后，电话回访客户询问产品质量状况，及时获得产品质量信息。对于客户对我公司产品提出质量异议，公司在接到异议后24小时内，作出处理意见。对用户反馈的产品质量问题及处理结果，我公司将予以存档。</p>
			<p>3、总经理组织质量部、市场部等相关人员不定期到客户处进行回访，虚心听取客户对我公司产品意见、建议。市场部年底发出客户满意调查表收集客户对公司质量、交期等方面的评价、建议。市场部对客户回访及调查情况进行汇总、分析，质量负责人组织原因分析，持续改进，增强顾客满意。</p>
			<p>4、我公司以“质量为先，顾客满意”的原则，严格内部管理，从原材料进厂到产品出厂，严格按质量体系文件规定的程序要求执行，并设立专职检验员严把产品质量关。</p>
			<div class="line"></div>
			<div class="title">售后服务流程图</div>
			<img src="/Public/index/img/sh2.jpg" alt="" />
		</div>
		<div class="aboutaddress"></div>
	</div>
</div>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
</script>

<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>