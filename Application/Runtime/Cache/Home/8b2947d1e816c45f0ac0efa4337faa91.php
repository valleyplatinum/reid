<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="<?php echo U('Index/index');?>">首页</a></li>
				<li><a href="<?php echo U('About/index');?>">走进瑞德</a></li>
				<li><a href="<?php echo U('Product/index');?>">产品展示</a></li>
				<li><a href="<?php echo U('Tech/index');?>">设备展示</a></li>
				<li><a href="<?php echo U('Quality/index');?>">品质管理</a></li>
				<li><a href="<?php echo U('After/index');?>">售后服务</a></li>
				<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="http://testing.com/">中文</a></li>
						<li><a href="http://en.testing.com/">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/about-banner.jpg" alt="" />
		</div>
	</div>
</div>
<div class="center">
	<div class="catposBox">
		<a href="#">首页</a>
		<em>></em>
		<span>走进瑞德</span>
	</div>
	<div class="aboutCenter">
		<div class="innerNav">
			<a href="<?php echo U('About/index');?>">公司简介</a>
			<a href="<?php echo U('About/culture');?>">企业文化</a>
			<a href="<?php echo U('About/organization');?>">组织机构</a>
			<a href="<?php echo U('About/certifications');?>">资质证书</a>
			<a href="<?php echo U('News/index');?>" class="on">媒体中心</a>
			<a href="<?php echo U('About/speech');?>">总经理致辞</a>
		</div>

		<div class="aboutmain">
			<div class="title"><?php echo ($arr["title"]); ?></div>
			<p><?php echo ($arr["text"]); ?></p>
		</div>
		<div class="aboutaddress"></div>
	</div>

</div>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
</script>
<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>