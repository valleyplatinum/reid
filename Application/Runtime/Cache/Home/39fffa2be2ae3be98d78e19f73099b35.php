<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="http://testing.com/">首页</a></li>
				<li><a href="http://testing.com/About/index">走进瑞德</a></li>
				<li><a href="http://testing.com/Product/index">产品展示</a></li>
				<li><a href="http://testing.com/Tech/index">设备展示</a></li>
				<li><a href="http://testing.com/Quality/index">品质管理</a></li>
				<li><a href="http://testing.com/After/index">售后服务</a></li>
				<li><a href="http://testing.com/Contact/index">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="http://testing.com/">中文</a></li>
						<li><a href="http://en.testing.com/">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/pj-banner.jpg" alt="" />
		</div>
	</div>
</div>
<div class="center">
	<div class="catposBox">
		<a href="#">首页</a>
		<em>></em>
		<span>品质管理</span>
	</div>
	<div class="aboutCenter">
		<div class="innerNav">
			<a href="<?php echo U('Quality/index');?>">品质管理与控制</a>
			<a href="<?php echo U('Quality/process');?>"  class="on">质量控制流程图</a>
			<a href="<?php echo U('Quality/itroduction');?>">生产工序简介</a>
		</div>

		<div class="pjMain">
			<img src="/Public/index/img/pj-img1.png" alt="" />
			<p>公司按照ISO9001标准建立了质量体系，并通过认证中心的审核获得证书。公司各部门严格执行工厂《质量手册》、《程序文件》等质量文件及技术文件的各项规定和要求，认真履行质量职责，有效开展各项活动。</p>
			<h5>一、合同转化</h5>
			<p class="p2">销售部按评审后的合同编制《生产任务单》，技术部门按合同要求编制图纸明确产品各项技术要求，根据材质、性能要求制定下料工艺、锻造工艺、热处理工艺，并按要求审批下发。</p>
			<h5>二、采购产品的控制和验证</h5>
			<p class="p2">原材料入厂复验：原材料到厂后，对原材料规格尺寸、表面质量、标识（主要指材质、炉号）进行检验，合格后理化室进行PMI检测和力学性能测试。</p>
			<h5>三、生产过程控制:</h5>
			<p class="p2">
				检验员检验每步工序，检验合格的产品凭检验员签字的《随件流动卡》转序，不合格产品按公司程序文件《不合格品控制程序》执行。<br>
				1、锯切下料：严格按《下料任务单》锯切下料，并做检验记录，合格后方能批量锯切。<br>
				2、加热：不同材质、不同炉号的产品不能同炉加热。<br>
				3、锻造：操作人员按照图纸进行锻造，锻造过程中用红外线测温仪控制锻件锻造温度。<br>
				4、热处理：按周期检定热电偶、温度仪表。严格按热处理工艺规定的升温速度、保温温度、保温时间等工艺参数及冷却方式进行处理。<br>
				5、机加：<br>
				车加工：按照图纸进行生产，首检合格后进行批量生产。加工过程中检验员进行巡检，对加工的产品进行抽检。<br>
				6、包装前检验：由专职检验员严格按图纸对机加工后的产品尺寸、表面质量、标识进行检查，然后对产品进行PMI再检测，防止材质混淆。<br>
				7、包装工序：
			</p>
			<p class="p4">（1）打字：依据《生产任务单》中打字通知单进行打字，首件自检后，专职包装检验员进行首件检验，合格后进行批量打字，过程中对字迹清晰度进行抽检，保证打字清晰整齐。<br>
				（2）防锈处理：按客户要求进行防锈处理(涂油、蘸漆等)，操作工按要求自检，专职包装检验员按要求对防锈处理情况、表面质量进行首件检验、巡检、工序检，合格进入装箱工序，否则返工处理。</p>
			<h5>四、不合格品控制：</h5>
			<p class="p2">对出现的不合格品，检验员进行标识并隔离，填写《不合格处理单》交授权人员处理，严格按《不合格品控制程序》执行，确保不合格品得到识别和控制，防止其非预期使用或交付。</p>
		</div>
	</div>

</div>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
</script>

<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>