<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="<?php echo U('Index/index');?>">首页</a></li>
				<li><a href="<?php echo U('About/index');?>">走进瑞德</a></li>
				<li><a href="<?php echo U('Product/index');?>">产品展示</a></li>
				<li><a href="<?php echo U('Tech/index');?>">设备展示</a></li>
				<li><a href="<?php echo U('Quality/index');?>">品质管理</a></li>
				<li><a href="<?php echo U('After/index');?>">售后服务</a></li>
				<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="http://testing.com/">中文</a></li>
						<li><a href="http://en.testing.com/">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/about-banner.jpg" alt="" />
		</div>
	</div>
</div>
<div class="center">
	<div class="catposBox">
		<a href="#">首页</a>
		<em>></em>
		<span>走进瑞德</span>
	</div>
	<div class="aboutCenter">
		<div class="innerNav">
			<a href="<?php echo U('About/index');?>" >公司简介</a>
			<a href="<?php echo U('About/culture');?>">企业文化</a>
			<a href="<?php echo U('About/organization');?>">组织机构</a>
			<a href="<?php echo U('About/certifications');?>" class="on">资质证书</a>
			<a href="<?php echo U('News/index');?>">媒体中心</a>
			<a href="<?php echo U('About/speech');?>">总经理致辞</a>
		</div>
		<div class="aboutmain">
			<p>山西瑞德机械制造有限公司是一家专业生产换热器管板、法兰 、锻件的股份制企业，2014年成立，公司占地面积15000平方米，注册资金960万元，总投资2500万元，现有员工100余人，其中高、中级技术人员30人，具备下料、锻造、热处理、机加工、理化检测、产品表面处理、标记、包装、发运成套能力。主要生产设备有10T锻造锤2部、D53碾环机1台、RX3热处理炉2台、C5125/C5250数控车6台、CM4200/2高速数控龙门钻2台、PM3000龙门加工中心2台、CNC加工中心、线切割、数控等离子切割、深孔镗等共计80余台生产设备。可生产国际、国家和行业标准的法兰管板锻件产品，也可生产由顾客提供图纸的各种异型法兰管板锻件产品。</p>
			<p>公司始终坚持“诚信高效，质量至上”的企业精神，率先在同行业中建立起质量保证体系，通过并取得德国TUV NORD公司的IS0-9001:2015质量管理体系证书、PED（2014/68/EU）证书、 AD 2000-Merkblatt W0证书；通过并取得中华人民共和国特种设备制造许可证（压力管道），另公司具有自主进出口贸易资格。</p>
			<p>公司始终秉承着 “诚信、高效、优质、合作、共赢、发展”的经营理念，开拓创新，不断进取，精心服务客户，使公司走上一条良性的发展之路。</p>
		</div>
		<div class="about-horner">
			<div class="swiper-slide">
				<div class="img hornerList">
					<a href="img/horner1.jpg"><img src="/Public/index/img/horner1.jpg" alt="" /></a>
				</div>
				<p>营业执照</p>
			</div>
			<div class="swiper-slide">
				<div class="img hornerList">
					<a href="img/horner2.jpg"><img src="/Public/index/img/horner2.jpg" alt="" /></a>
				</div>
				<p>特种设备制造许可证</p>
			</div>
			<div class="swiper-slide">
				<div class="img hornerList">
					<a href="img/horner3.jpg"><img src="/Public/index/img/horner3.jpg" alt="" /></a>
				</div>
				<p>9001质量体系认证</p>
			</div>
			<div class="swiper-slide">
				<div class="img hornerList">
					<a href="img/horner4.jpg"><img src="/Public/index/img/horner4.jpg" alt="" /></a>
				</div>
				<p>9001质量体系认证</p>
			</div>
		</div>
	</div>

</div>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: false,
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	$('.about-horner').rebox({ selector: '.hornerList a' });
</script>

<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>