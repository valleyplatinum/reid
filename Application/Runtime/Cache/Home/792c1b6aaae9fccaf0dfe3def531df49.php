<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
		<meta name="renderer" content="webkit">
		<title><?php echo ($web_title); ?></title>
		<meta name="keywords" content="<?php echo ($web_description); ?>">
		<meta name="description" content="<?php echo ($web_text); ?>">
		<LINK rel="Bookmark" href="/Public/favicon.ico" >
		<LINK rel="Shortcut Icon" href="/Public/favicon.ico" />

		<link rel="stylesheet" type="text/css" href="/Public/wap/css/swiper.min.css" />
		<link rel="stylesheet" href="/Public/wap/css/jquery-rebox.css">
		<!--WebSiteCss-->
		<link rel="stylesheet" type="text/css" href="/Public/wap/css/MobileSiteStyle.css">
		<!--WebSiteJs-->
		<script src="/Public/wap/js/jquery-2.1.4.min.js"></script>			
		<script src="/Public/wap/js/jquery-rebox.js"></script> 
	</head>
<body>
<input type="hidden" value="" id="app">
<input type="hidden" value="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>" id="host">
<input type="hidden" value="<?php echo CONTROLLER_NAME ?>" class="con">
<input type="hidden" value="<?php echo ACTION_NAME ?>" class="mod">
<!--移动端导航展开-->
<div class="headerBox">
	<div class="logo">
		<a href="<?php echo U('/');?>"><img src="/Public/wap/img/logo.png"></a>
	</div>
	<div class="nav">
		<div class="top-line"></div>
		<div class="middle-line"></div>
		<div class="bottom-line"></div>
	</div>
	<div id="overlayNav">
		<div id="mobileNavWrapper" class="nav-wrapper">
			<ul class="navitem">
				<?php if(is_array($Category)): foreach($Category as $key=>$val): if($val['parent'] == 0 && $val['display'] == 1): ?><li><a href='<?php echo U("$val[url]");?>' class="fu"><?php echo ($val['title']); ?></a></li><?php endif; endforeach; endif; ?>
			</ul>
		</div>
	</div>
	<div class="search-box"></div>
	<div class="language">
		<?php  if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){ echo '<span class="en">EN</span>'; }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){ echo '<span class="cn">CN</span>'; } ?>
	</div>				
</div>
<div class="search">
	<div class="search-input">
		<input type="text" class="ipt" placeholder="<?php echo ($lang->index('11')); ?>" id="kw">
		<input type="submit" class="tj" value="">
	</div>
</div>
<div class="slideBox-inner" style="background:url(/Public/wap/img/about1.jpg) no-repeat center center;background-size:cover;">
	<div class="font animated fadeInDown">
		<?php  if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){ echo '<div class="p"><span>科技领先</span><span>质量第一</span><span>用户至上</span></div>'; echo '<p>Science and technology leading</p><p>Quality first</p><p>Customer first</p>'; }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){ echo '<p>Science and technology leading</p><p>Quality first</p><p>Customer first</p>'; } ?>
	</div>				
</div>
<div class="aboutWapper1">
	<img src="/Public/wap/img/about2.jpg" alt="" />
	<div class="main">
		<p><?php echo ($About["left"]); ?></p>
		<p><?php echo ($About["right"]); ?></p>
	</div>
</div>

<div class="indexWapper3 about3">
	<div class="title"><?php echo ($lang->index('21')); ?></div>
	<div class="swiper-container Team">
	  	<div class="swiper-wrapper">
	  		<?php $img1=explode(',',$About['img1']); ?>
	   		<?php if(is_array($img1)): foreach($img1 as $key=>$val): ?><div class="swiper-slide">
		    		<img src="<?php echo ($val); ?>" alt="" />
		   		</div><?php endforeach; endif; ?>
	  	</div>
	  	<!-- Add Pagination -->
	  	<div class="swiper-pagination"></div>
	</div>
</div>
<div class="indexWapper3 about3">
	<div class="title">厂区厂貌</div>
	<div class="swiper-container environment">
	  	<div class="swiper-wrapper">
	  		<?php $img2=explode(',',$About['img2']); ?>
	   		<?php if(is_array($img2)): foreach($img2 as $key=>$val): ?><div class="swiper-slide">
		    		<img src="<?php echo ($val); ?>" alt="" />
		   		</div><?php endforeach; endif; ?>
	  	</div>
	  	<!-- Add Pagination -->
	  	<div class="swiper-pagination"></div>
	</div>
</div>
<div class="footerWapper">
	<ul>
		<li><span><?php echo ($lang->index('6')); ?></span>：<?php echo ($Contact["tel"]); ?></li>
		<li><span><?php echo ($lang->index('7')); ?></span>：<?php echo ($Contact["fax"]); ?></li>
		<li><span><?php echo ($lang->index('8')); ?></span>：<?php echo ($Contact["email"]); ?></li>
		<li><span><?php echo ($lang->index('9')); ?></span>：<?php echo ($Contact["addr"]); ?></li>
	</ul>
	<p><?php echo ($lang->index('12')); ?> <?php echo ($Web["banquan"]); ?></p>
</div>
<script src="/Public/wap/js/swiper-4.1.6.min.js"></script>	
<script src="/Public/wap/js/MobileSiteJs.js"></script>
</body>
</html>
<script>
	new Swiper('.Team', {
	 	pagination: {
	    	el: '.swiper-pagination',
	    	clickable: true,
	  	},
	});
	new Swiper('.environment', {
	  	pagination: {
	   	 	el: '.swiper-pagination',
	    	clickable: true,
	  	},
	});
</script>