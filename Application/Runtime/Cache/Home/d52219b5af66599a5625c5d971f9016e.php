<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo ($web_title); ?></title>
    <meta name="keywords" content="<?php echo ($web_description); ?>">
    <meta name="description" content="<?php echo ($web_text); ?>">
    <LINK rel="Bookmark" href="/Public/favicon.ico" >
    <LINK rel="Shortcut Icon" href="/Public/favicon.ico" />
    <!--WebSiteCss-->
    <link rel="stylesheet" href="/Public/index/css/animate.css">
    <!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />		
	<link rel="stylesheet" href="/Public/index/css/jquery.zyslide.css">
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
    <link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">    	
    <!--WebSiteJs-->
    <script src="/Public/index/js/jquery-2.1.4.min.js"></script>
    <script src="/Public/index/js/jquery.SuperSlide.2.1.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>     
	<script src="/Public/index/js/jquery.zyslide.js"></script>
</head>
<body>
	<input type="hidden" value="" id="app">
	<input type="hidden" value="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>" id="host">
	<input type="hidden" value="<?php echo CONTROLLER_NAME ?>" class="con">
	<input type="hidden" value="<?php echo ACTION_NAME ?>" class="mod">

	<div class="header-box">
		<div class="header">
			<div class="logo">
				<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
			</div>
			<div class="nav-box">
				<ul class="nav">
					<?php if(is_array($Category)): foreach($Category as $key=>$val): if($val['parent'] == 0 && $val['display'] == 1): ?><li><a href='<?php echo U("$val[url]");?>' class="fu"><?php echo ($val['title']); ?></a></li><?php endif; endforeach; endif; ?>
				</ul>
				<div class="nav2">
					<div class="search-box">
						<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
						<div class="search">
							<div class="search-input">
								<input type="text" class="ipt" placeholder="<?php echo ($lang->index('11')); ?>" id="kw">
								<input type="submit" class="tj" value="">
							</div>
						</div>
					</div>					
					<div class="nav2-span">
						<span class="cn">
							<?php  if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){ echo $lang->index('13'); }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){ echo $lang->index('14'); } ?>
						</span>
						<em><img src="/Public/index/img/jt.png" alt=""></em>
						<ul class="lan">
							<li id="cn"><a href="javascript:;"><?php echo ($lang->index('13')); ?></a></li>
							<li id="en"><a href="javascript:;"><?php echo ($lang->index('14')); ?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="slideBox-inner" style="background:url(/Public/index/img/about1.jpg) no-repeat center center;background-size:cover;">
	<div class="font animated fadeInDown">
		<div class="p"><span>科技领先</span><span>质量第一</span><span>用户至上</span></div>
		<p>Science and technology leading</p>
		<p>Quality first</p>
		<p>Customer first</p>
	</div>				
</div>
	<div class="newsWapper">
		<div class="newsMain">
			<div class="left" style="font-size:18px;">
				<?php echo ($lang->index('35')); ?>
			</div>
		</div>
	</div>
	<div class="footer-box">
		<div class="footer1">
			<div class="footer1-box">
				<div class="footer1-1">
					<h2><img src="/Public/index/img/flogo.png" alt="" /></h2>
					<ul>
						<li><span><?php echo ($lang->index('6')); ?></span>：<?php echo ($Contact["tel"]); ?></li>
						<li><span><?php echo ($lang->index('7')); ?></span>：<?php echo ($Contact["fax"]); ?></li>
						<li><span><?php echo ($lang->index('8')); ?></span>：<?php echo ($Contact["email"]); ?></li>
						<li><span><?php echo ($lang->index('9')); ?></span>：<?php echo ($Contact["addr"]); ?></li>
					</ul>
				</div>
				<div class="footer1-2">
					<div class="footer1-2-top">
						<div class="bdsharebuttonbox">
							<a href="#" class="bds_weixin wx" data-cmd="weixin" title="<?php echo ($lang->index('15')); ?>"></a>
							<a href="#" class="bds_sqq tencent" data-cmd="sqq" title="<?php echo ($lang->index('16')); ?>"></a>
							<a href="#" class="bds_tsina blog" data-cmd="tsina" title="<?php echo ($lang->index('17')); ?>"></a>
							<a href="#" class="bds_tqq sy" data-cmd="tqq" title="<?php echo ($lang->index('18')); ?>"></a>
							<a href="#" class="bds_qzone qq" data-cmd="qzone" title="<?php echo ($lang->index('19')); ?>"></a>
           				</div>
					</div>
					<div class="footer1-2-box">
						<input type="text" class="ipt" id="kw2">
						<input type="submit" class="tj" value="<?php echo ($lang->index('10')); ?>" id="tj">
					</div>
					<div class="copyright"><?php echo ($lang->index('12')); ?> <?php echo ($Web["banquan"]); ?></div>
				</div>
			</div>			
		</div>
	</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
</script>
<script src="/Public/index/js/PcSiteJs.js"></script>	
</html>