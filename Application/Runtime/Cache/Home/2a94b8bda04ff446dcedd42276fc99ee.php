<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="<?php echo U('Index/index');?>">首页</a></li>
				<li><a href="<?php echo U('About/index');?>">走进瑞德</a></li>
				<li><a href="<?php echo U('Product/index');?>">产品展示</a></li>
				<li><a href="<?php echo U('Tech/index');?>">设备展示</a></li>
				<li><a href="<?php echo U('Quality/index');?>">品质管理</a></li>
				<li><a href="<?php echo U('After/index');?>">售后服务</a></li>
				<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="http://testing.com/">中文</a></li>
						<li><a href="http://en.testing.com/">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/banner1.jpg" alt="" />
			<div class="font font1" >
				<div class="f1"><img src="/Public/index/img/b-font1.png" alt="" /></div>
				<div class="f2">专注法兰锻件制造</div>
				<div class="f3">20 years dedicated to the manufacture of flange forgings</div>
			</div>

		</div>
		<div class="swiper-slide">
			<img src="/Public/index/img/banner2.jpg" alt="" />
			<div class="font font2">
				<div class="f1"><img src="/Public/index/img/b-font2.png" alt="" /></div>
				<div class="f3">Atten tively,make quality ascension endless!</div>
			</div>
		</div>
		<div class="swiper-slide">
			<img src="/Public/index/img/banner3.jpg" alt="" />
			<div class="font font3">
				<div class="f1"><img src="/Public/index/img/b-font3.png" alt="" /></div>
				<div class="f4">换热器管板 法兰 锻件 专业制造厂家</div>
				<ul>
					<li><img src="/Public/index/img/b-icon1.png" alt="" /><p>品质保证</p></li>
					<li><img src="/Public/index/img/b-icon2.png" alt="" /><p>优质材料</p></li>
					<li><img src="/Public/index/img/b-icon3.png" alt="" /><p>专业制造</p></li>
					<li><img src="/Public/index/img/b-icon4.png" alt="" /><p>规格多样</p></li>
					<li><img src="/Public/index/img/b-icon5.png" alt="" /><p>库存充足</p></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- Add Pagination -->
	<div class="swiper-pagination"></div>
</div>
<div class="index-content index-content1">
	<div class="center">
		<div class="indexTitle" aos="fade-up" aos-duration="800" aos-delay="200">
			<div><p>产品展示</p><span>PRODUCT DISPLAY</span></div>
		</div>
		<ul class="proList">
			<li aos="fade-up" aos-duration="800" aos-delay="400">
				<a href="#">
					<div class="img">
						<img src="/Public/index/img/pro1.png" alt="" />
					</div>
					<p>活套法兰</p>
				</a>
			</li>
			<li aos="fade-up" aos-duration="800" aos-delay="400">
				<a href="#">
					<div class="img">
						<img src="/Public/index/img/pro2.png" alt="" />
					</div>
					<p>活套法兰</p>
				</a>
			</li>
			<li aos="fade-up" aos-duration="800" aos-delay="400">
				<a href="#">
					<div class="img">
						<img src="/Public/index/img/pro3.png" alt="" />
					</div>
					<p>活套法兰</p>
				</a>
			</li>
			<li aos="fade-up" aos-duration="800" aos-delay="400">
				<a href="#">
					<div class="img">
						<img src="/Public/index/img/pro4.png" alt="" />
					</div>
					<p>活套法兰</p>
				</a>
			</li>
			<li aos="fade-up" aos-duration="800" aos-delay="600">
				<a href="#">
					<div class="img">
						<img src="/Public/index/img/pro5.png" alt="" />
					</div>
					<p>活套法兰</p>
				</a>
			</li>
			<li aos="fade-up" aos-duration="800" aos-delay="600">
				<a href="#">
					<div class="img">
						<img src="/Public/index/img/pro6.png" alt="" />
					</div>
					<p>活套法兰</p>
				</a>
			</li>
			<li aos="fade-up" aos-duration="800" aos-delay="600">
				<a href="#">
					<div class="img">
						<img src="/Public/index/img/pro7.png" alt="" />
					</div>
					<p>活套法兰</p>
				</a>
			</li>
			<li aos="fade-up" aos-duration="800" aos-delay="600">
				<a href="#">
					<div class="img">
						<img src="/Public/index/img/pro8.png" alt="" />
					</div>
					<p>活套法兰</p>
				</a>
			</li>
		</ul>
		<a href="#" class="more">展示更多</a>
	</div>
</div>
<div class="index-content index-content2">
	<div class="center">
		<div class="indexTitle bj" aos="fade-up" aos-duration="800" aos-delay="200">
			<div><p>资质证书</p><span>QUALIFICATION CERTIFICATE</span></div>
		</div>
		<a href="javascript:;" class="control left" aos="fade-up" aos-duration="800" aos-delay="400"></a>
		<a href="javascript:;" class="control right" aos="fade-up" aos-duration="800" aos-delay="400"></a>
		<div class="swiper-container horner" aos="fade-up" aos-duration="800" aos-delay="400">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="img hornerList">
						<a href="img/horner1.jpg"><img src="/Public/index/img/horner1.jpg" alt="" /></a>
					</div>
					<p>营业执照</p>
				</div>
				<div class="swiper-slide">
					<div class="img hornerList">
						<a href="img/horner2.jpg"><img src="/Public/index/img/horner2.jpg" alt="" /></a>
					</div>
					<p>特种设备制造许可证</p>
				</div>
				<div class="swiper-slide">
					<div class="img hornerList">
						<a href="img/horner3.jpg"><img src="/Public/index/img/horner3.jpg" alt="" /></a>
					</div>
					<p>9001质量体系认证</p>
				</div>
				<div class="swiper-slide">
					<div class="img hornerList">
						<a href="img/horner4.jpg"><img src="/Public/index/img/horner4.jpg" alt="" /></a>
					</div>
					<p>9001质量体系认证</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="index-content index-content3">
	<div class="center">
		<div class="indexTitle" aos="fade-up" aos-duration="800" aos-delay="200">
			<div><p>公司简介</p><span>CONPANY PROFILE</span></div>
		</div>
		<div class="aboutposter" aos="fade-up" aos-duration="800" aos-delay="400">
			<img src="/Public/index/img/about.jpg" alt="" />
			<a href="#">了解更多&nbsp;&nbsp;&nbsp;&nbsp;></a>
		</div>
	</div>
</div>
<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>
<!--效果-->
<script src="js/aos.js"></script>
<script src="js/swiper-4.1.6.min.js"></script>
<script src="js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>