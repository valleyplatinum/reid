<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="<?php echo U('Index/index');?>">首页</a></li>
				<li><a href="<?php echo U('About/index');?>">走进瑞德</a></li>
				<li><a href="<?php echo U('Product/index');?>">产品展示</a></li>
				<li><a href="<?php echo U('Tech/index');?>">设备展示</a></li>
				<li><a href="<?php echo U('Quality/index');?>">品质管理</a></li>
				<li><a href="<?php echo U('After/index');?>">售后服务</a></li>
				<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="http://testing.com/">中文</a></li>
						<li><a href="http://en.testing.com/">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/pj-banner.jpg" alt="" />
		</div>
	</div>
</div>
<div class="center">
	<div class="catposBox">
		<a href="#">首页</a>
		<em>></em>
		<span>品质管理</span>
	</div>
	<div class="aboutCenter">
		<div class="innerNav">
			<a href="<?php echo U('Quality/index');?>" class="on">品质管理与控制</a>
			<a href="<?php echo U('Quality/process');?>">质量控制流程图</a>
			<a href="<?php echo U('Quality/itroduction');?>">生产工序简介</a>
		</div>

		<div class="pjMain">
			<h6>品质管理与控制</h6>
			<p>在原材料、制造环节、检验检测、发货过程中，都严守规范，做好过程控制并及时出具各种报告，分析其根源，以便保持持续改善，毫不妥协的执行追溯性与一致性品质的管理规范。</p>
			<div class="line"></div>
			<p>我们在过程控制中倾注了大量心血，持续并毫不妥协的执行追溯性与一致性品质的管理规范.</p>
			<div class="line"></div>
			<h6>内控品质检测：</h6>
			<p> · 光谱仪检测分析化学元素<br>
				· 金相分析 ·机械性能检测（硬度、拉力、屈服度、延展率、缩减率、冲击等）<br>
				· 超声波<br>
				· 便携式直读光普仪<br>
				· 宏观侵蚀测试<br>
				· 晶间腐蚀测试 <br>
				· 渗透测试<br>
				· 磁粉探伤<br>
				· 尺寸检验<br>
				· 外观检验<br>
				* 其它由客户规定的特殊测试，我们将按照客户的要求执行，并积极配合 第三方检诱工做。
			</p>
		</div>
	</div>

</div>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
</script>

<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>