<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="<?php echo U('Index/index');?>">首页</a></li>
				<li><a href="<?php echo U('About/index');?>">走进瑞德</a></li>
				<li><a href="<?php echo U('Product/index');?>">产品展示</a></li>
				<li><a href="<?php echo U('Tech/index');?>">设备展示</a></li>
				<li><a href="<?php echo U('Quality/index');?>">品质管理</a></li>
				<li><a href="<?php echo U('After/index');?>">售后服务</a></li>
				<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="http://testing.com/">中文</a></li>
						<li><a href="http://en.testing.com/">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/banner3.jpg" alt="" />
			<div class="font font3">
				<div class="f1"><img src="/Public/index/img/b-font3.png" alt="" /></div>
				<div class="f4">换热器管板 法兰 锻件 专业制造厂家</div>
				<ul>
					<li><img src="/Public/index/img/b-icon1.png" alt="" /><p>品质保证</p></li>
					<li><img src="/Public/index/img/b-icon2.png" alt="" /><p>优质材料</p></li>
					<li><img src="/Public/index/img/b-icon3.png" alt="" /><p>专业制造</p></li>
					<li><img src="/Public/index/img/b-icon4.png" alt="" /><p>规格多样</p></li>
					<li><img src="/Public/index/img/b-icon5.png" alt="" /><p>库存充足</p></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="center">
	<div class="catposBox">
		<a href="#">首页</a>
		<em>></em>
		<span>品质管理</span>
	</div>
	<div class="aboutCenter">
		<div class="innerNav">
			<div class="innerNav">
				<a href="<?php echo U('Quality/index');?>">品质管理与控制</a>
				<a href="<?php echo U('Quality/process');?>">质量控制流程图</a>
				<a href="<?php echo U('Quality/itroduction');?>"  class="on">生产工序简介</a>
			</div>
		</div>

		<div class="pjMain">
			<div class="item">
				<table>
					<tr>
						<td><img src="/Public/index/img/pj1.jpg" alt="" /></td>
						<td class="td1">
							<b>热加工</b>
							<p>加热炉持续工作的天然气加热炉，精准高效，使料块升温至适宜锻打温度</p>
						</td>
					</tr>
				</table>
			</div>
			<div class="item">
				<table>
					<tr>
						<td><img src="/Public/index/img/pj2.jpg" alt="" /></td>
						<td class="td1">
							<b>锻造锤打</b>
							<p>由空气锤，辗环机，压力机组合成的锻 造生产线，适合法兰行业的多规格，少数量的制造。柔性化生产，可以轻而易举完成客户订单，缩短交期。</p>
						</td>
					</tr>
				</table>
			</div>
			<div class="item">
				<table>
					<tr>
						<td><img src="/Public/index/img/pj3.jpg" alt="" /></td>
						<td class="td1">
							<b>碾环 </b>
							<p>碾压各类无缝环形锻件，使粗毛坯成形，既保证了产品的精度，又节约了成本，使产品更具性价比</p>
						</td>
					</tr>
				</table>
			</div>
			<div class="item">
				<table>
					<tr>
						<td><img src="/Public/index/img/pj4.jpg" alt="" /></td>
						<td class="td1">
							<b>机加工</b>
							<p>全数控机加工的搞高效生产，即保证了表面光洁度，也使产品尺寸更精确，保证一致性。<br>
								台湾引进的高速数控钻使螺栓孔壁光滑无比，令人爱不释手。</p>
						</td>
					</tr>
				</table>
			</div>
			<div class="item">
				<table>
					<tr>
						<td><img src="/Public/index/img/pj5.jpg" alt="" /></td>
						<td class="td1">
							<b>防锈</b>
							<p>对已机加工完成的法兰表面，实施防锈处理，常用方式为涂油，涂漆，镀锌和涂亚 硝酸等。<br>
								涂油  最为通行通用的防锈处理，便捷实用。<br>
								涂漆  黑漆，黄漆，蓝漆以及其它。其中黑漆与黄漆为通行通用的方式，蓝漆以及其它，按照客户特殊要求商定执行。<br>
								镀锌  一般采用热镀锌，使产品更防腐防锈，按照客户特殊要求执行。<br>
								涂亚硝酸  使用比较少，防锈周期比较短，一般约为1-2个星期，因此更多应用在生产周转中，而非发货产品。除非客户有此要求，经双方商定后可实施。</p>
						</td>
					</tr>
				</table>
			</div>
			<div class="item">
				<table>
					<tr>
						<td><img src="/Public/index/img/pj6.jpg" alt="" /></td>
						<td class="td1">
							<b>包装</b>
							<p>足够厚的板材保证了木箱的结实程度，每一层法兰都使用隔板防止碰撞，缝隙处使用挤塑板填充，<br>
								使整个包装浑然一体，结实坚固，保证产品从出厂到客户手中，保持始终如一的高品质。
							</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

</div>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: false,
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
</script>

<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>