<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo ($web_title); ?></title>
    <meta name="keywords" content="<?php echo ($web_description); ?>">
    <meta name="description" content="<?php echo ($web_text); ?>">
    <LINK rel="Bookmark" href="/Public/favicon.ico" >
    <LINK rel="Shortcut Icon" href="/Public/favicon.ico" />
    <!--WebSiteCss-->
    <link rel="stylesheet" href="/Public/index/css/animate.css">
    <!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />		
	<link rel="stylesheet" href="/Public/index/css/jquery.zyslide.css">
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
    <link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">    	
    <!--WebSiteJs-->
    <script src="/Public/index/js/jquery-2.1.4.min.js"></script>
    <script src="/Public/index/js/jquery.SuperSlide.2.1.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>     
	<script src="/Public/index/js/jquery.zyslide.js"></script>
</head>
<body>
	<input type="hidden" value="" id="app">
	<input type="hidden" value="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>" id="host">
	<input type="hidden" value="<?php echo CONTROLLER_NAME ?>" class="con">
	<input type="hidden" value="<?php echo ACTION_NAME ?>" class="mod">

	<div class="header-box">
		<div class="header">
			<div class="logo">
				<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
			</div>
			<div class="nav-box">
				<div class="nav2">
					<div class="search-box">
						<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
						<div class="search">
							<div class="search-input">
								<input type="text" class="ipt" placeholder="<?php echo ($lang->index('11')); ?>" id="kw">
								<input type="submit" class="tj" value="">
							</div>
						</div>
					</div>					
					<div class="nav2-span">
						<span class="cn">
							<?php  if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){ echo $lang->index('13'); }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){ echo $lang->index('14'); } ?>
						</span>
						<em><img src="/Public/index/img/jt.png" alt=""></em>
						<ul class="lan">
							<li id="cn"><a href="javascript:;"><?php echo ($lang->index('13')); ?></a></li>
							<li id="en"><a href="javascript:;"><?php echo ($lang->index('14')); ?></a></li>
						</ul>
					</div>
				</div>
				<ul class="nav">
					<?php if(is_array($Category)): foreach($Category as $key=>$val): if($val['parent'] == 0 && $val['display'] == 1): ?><li><a href='<?php echo U("$val[url]");?>' class="fu"><?php echo ($val['title']); ?></a></li><?php endif; endforeach; endif; ?>
				</ul>
				
			</div>
		</div>
	</div>
<div class="slideBox-inner" style="background:url(/Public/index/img/about1.jpg) no-repeat center center;background-size:cover;">
	<div class="font animated fadeInDown">
		<?php  if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){ echo '<div class="p"><span>科技领先</span><span>质量第一</span><span>用户至上</span></div>'; echo '<p>Science and technology leading</p><p>Quality first</p><p>Customer first</p>'; }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){ echo '<p style="font-size:25px;margin-bottom:15px">Science and technology leading</p><p style="font-size:25px;margin-bottom:15px">Quality first</p><p style="font-size:25px;margin-bottom:15px">Customer first</p>'; } ?>
	</div>				
</div>
<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>

<div class="index-content">
	<div class="catpos">
		<a href="<?php echo U('/');?>" class="home"><?php echo ($lang->index('20')); ?></a><em>></em>
		<a href=""><?php echo ($title); ?></a>
	</div>
	<div class="sub-content1">
		<div class="content-title blue">
			<h2>PROJECT CASE</h2>
			<h3><?php echo ($lang->index('34')); ?></h3>
		</div>
		<div class="sub-content1-main">
			<ul>
				<?php if(is_array($Next)): foreach($Next as $key=>$val): if($key % 2 == 0): ?><li class="li<?php echo ($key+1); ?> huan" aos="fade-up" aos-duration="800" aos-delay="400" index="<?php echo ($val["id"]); ?>"><a href="javascript:;"><span></span><p><?php echo ($val["title"]); ?></p><em>0<?php echo ($key+1); ?></em></a></li>
					<?php else: ?>
						<li class="li<?php echo ($key+1); ?> huan" aos="fade-down" aos-duration="800" aos-delay="400" index="<?php echo ($val["id"]); ?>"><a href="javascript:;"><span></span><p><?php echo ($val["title"]); ?></p><em>0<?php echo ($key+1); ?></em></a></li><?php endif; endforeach; endif; ?>
			</ul>
		</div>
	</div>
	<div class="sub-content2">
		<div class="sub2-top">
			<div class="sub2-lf">
				<p id="ty"><?php echo ($Next["0"]["title"]); ?></p>
				<span>RUIDE CASE</span>
			</div>
			<div class="sub2-rg">
				<?php echo ($Next["0"]["text"]); ?>
			</div>
		</div>
		<div class="swiper-container">
		    <div class="swiper-wrapper">
		    	<?php if(is_array($Field)): foreach($Field as $key=>$val): if($val[type] == 1): ?><div class="swiper-slide">
					      	<div class="img"><img src="/Uploads/img<?php echo ($val["img"]); ?>" alt="" /></div>
					      	<div class="title"><?php echo ($val["title"]); ?></div>
				      	</div><?php endif; endforeach; endif; ?>
		    </div>
		    <!-- Add Pagination -->
		    <div class="swiper-pagination"></div>
		    <!-- Add Arrows -->
		    <div class="swiper-button-next"></div>
		    <div class="swiper-button-prev"></div>
	  	</div>
	</div>
</div>
<!-- <script type="text/javascript" src="/Public/index/js/field_index.js"></script> -->
	<div class="footer-box">
		<div class="footer1">
			<div class="footer1-box">
				<div class="footer1-1">
					<h2><img src="/Public/index/img/flogo.png" alt="" /></h2>
					<ul>
						<li><span><?php echo ($lang->index('6')); ?></span>：<?php echo ($Contact["tel"]); ?></li>
						<li><span><?php echo ($lang->index('7')); ?></span>：<?php echo ($Contact["fax"]); ?></li>
						<li><span><?php echo ($lang->index('8')); ?></span>：<?php echo ($Contact["email"]); ?></li>
						<li><span><?php echo ($lang->index('9')); ?></span>：<?php echo ($Contact["addr"]); ?></li>
					</ul>
				</div>
				<div class="footer1-2">
					<div class="footer1-2-top">
						<div class="bdsharebuttonbox">
							<a href="#" class="bds_weixin wx" data-cmd="weixin" title="<?php echo ($lang->index('15')); ?>"></a>
							<a href="#" class="bds_sqq tencent" data-cmd="sqq" title="<?php echo ($lang->index('16')); ?>"></a>
							<a href="#" class="bds_tsina blog" data-cmd="tsina" title="<?php echo ($lang->index('17')); ?>"></a>
							<a href="#" class="bds_tqq sy" data-cmd="tqq" title="<?php echo ($lang->index('18')); ?>"></a>
							<a href="#" class="bds_qzone qq" data-cmd="qzone" title="<?php echo ($lang->index('19')); ?>"></a>
           				</div>
					</div>
					<div class="footer1-2-box">
						<input type="text" class="ipt" id="kw2">
						<input type="submit" class="tj" value="<?php echo ($lang->index('10')); ?>" id="tj">
					</div>
					<div class="copyright"><?php echo ($lang->index('12')); ?> <?php echo ($Web["banquan"]); ?></div>
				</div>
			</div>			
		</div>
	</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
</script>
<script src="/Public/index/js/PcSiteJs.js"></script>	
</html>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	var swiper = new Swiper('.swiper-container', {
	 	effect:'fade',
	    slidesPerView: 1,
	    // autoHeight: true,
	    pagination: {
      		el: '.swiper-pagination',
      		clickable: true,
    	},
    	navigation: {
      		nextEl: '.swiper-button-next',
      		prevEl: '.swiper-button-prev',
    	},
  	});

  	$('.nav').find('li').eq(3).attr('class','on');

  	var app=$('#app').val();

	$('.huan').click(function(){
		var id=$(this).attr('index');
		$.ajax({
			type:'POST',
			url:app+'/Field/find',
			data:{id:id},
			dataType:'json',
			success:function(data){
				if(data.code == 'ok'){
					$('.swiper-wrapper').html(data.img);
					$('#ty').text(data.title);
					$('.sub2-rg').text(data.text);
					var swiper = new Swiper('.swiper-container', {
					 	effect:'fade',
					    slidesPerView: 1,
					    // autoHeight: true,
					    pagination: {
				      		el: '.swiper-pagination',
				      		clickable: true,
				    	},
				    	navigation: {
				      		nextEl: '.swiper-button-next',
				      		prevEl: '.swiper-button-prev',
				    	},
				  	});
				}
			}
		})
	})
</script>