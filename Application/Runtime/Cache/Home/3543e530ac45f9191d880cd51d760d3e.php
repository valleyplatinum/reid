<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
		<meta name="renderer" content="webkit">
		<title><?php echo ($web_title); ?></title>
		<meta name="keywords" content="<?php echo ($web_description); ?>">
		<meta name="description" content="<?php echo ($web_text); ?>">
		<LINK rel="Bookmark" href="/Public/favicon.ico" >
		<LINK rel="Shortcut Icon" href="/Public/favicon.ico" />

		<link rel="stylesheet" type="text/css" href="/Public/wap/css/swiper.min.css" />
		<link rel="stylesheet" href="/Public/wap/css/jquery-rebox.css">
		<!--WebSiteCss-->
		<link rel="stylesheet" type="text/css" href="/Public/wap/css/MobileSiteStyle.css">
		<!--WebSiteJs-->
		<script src="/Public/wap/js/jquery-2.1.4.min.js"></script>			
		<script src="/Public/wap/js/jquery-rebox.js"></script> 
	</head>
<body>
<input type="hidden" value="" id="app">
<input type="hidden" value="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>" id="host">
<input type="hidden" value="<?php echo CONTROLLER_NAME ?>" class="con">
<input type="hidden" value="<?php echo ACTION_NAME ?>" class="mod">
<!--移动端导航展开-->
<div class="headerBox">
	<div class="logo">
		<a href="<?php echo U('/');?>"><img src="/Public/wap/img/logo.png"></a>
	</div>
	<div class="nav">
		<div class="top-line"></div>
		<div class="middle-line"></div>
		<div class="bottom-line"></div>
	</div>
	<div id="overlayNav">
		<div id="mobileNavWrapper" class="nav-wrapper">
			<ul class="navitem">
				<?php if(is_array($Category)): foreach($Category as $key=>$val): if($val['parent'] == 0 && $val['display'] == 1): ?><li><a href='<?php echo U("$val[url]");?>' class="fu"><?php echo ($val['title']); ?></a></li><?php endif; endforeach; endif; ?>
			</ul>
		</div>
	</div>
	<div class="search-box"></div>
	<div class="language">
		<?php  if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){ echo '<span class="en">EN</span>'; }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){ echo '<span class="cn">CN</span>'; } ?>
	</div>				
</div>
<div class="search">
	<div class="search-input">
		<input type="text" class="ipt" placeholder="<?php echo ($lang->index('11')); ?>" id="kw">
		<input type="submit" class="tj" value="">
	</div>
</div>
<div class="slideBox-inner" style="background:url(/Public/wap/img/about1.jpg) no-repeat center center;background-size:cover;">
	<div class="font animated fadeInDown">
		<?php  if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){ echo '<div class="p"><span>科技领先</span><span>质量第一</span><span>用户至上</span></div>'; echo '<p>Science and technology leading</p><p>Quality first</p><p>Customer first</p>'; }elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){ echo '<p>Science and technology leading</p><p>Quality first</p><p>Customer first</p>'; } ?>
	</div>				
</div>
<div class="map">
	<div style="width:100%;height:100%;" id="dituContent"></div>
</div>
<div class="address">
	<div class="top">
		<div class="name"><?php echo ($Contact["company"]); ?></div>
		<div class="add"><?php echo ($lang->index('9')); ?>：<?php echo ($Contact["addr"]); ?></div>
		<p><span><?php echo ($lang->index('6')); ?>：</span><?php echo ($Contact["tel"]); ?></p>
		<p><span><?php echo ($lang->index('7')); ?>：</span><?php echo ($Contact["fax"]); ?></p>
		<p><span><?php echo ($lang->index('8')); ?>：</span><?php echo ($Contact["email"]); ?></p>
		<p><span><?php echo ($lang->index('9')); ?>：</span><?php echo ($Contact["addr"]); ?></p>
	</div>
	<div class="bottom">
		<strong><?php echo ($lang->index('29')); ?></strong>
		<p><?php echo ($Contact["name"]); ?></p>
		<p><?php echo ($lang->index('6')); ?>：<?php echo ($Contact["tel2"]); ?></p>
		<p><?php echo ($lang->index('8')); ?>：<?php echo ($Contact["email2"]); ?></p>
		<p>QQ：<?php echo ($Contact["qq"]); ?></p>
	</div>
</div>
<div class="footerWapper">
	<ul>
		<li><span><?php echo ($lang->index('6')); ?></span>：<?php echo ($Contact["tel"]); ?></li>
		<li><span><?php echo ($lang->index('7')); ?></span>：<?php echo ($Contact["fax"]); ?></li>
		<li><span><?php echo ($lang->index('8')); ?></span>：<?php echo ($Contact["email"]); ?></li>
		<li><span><?php echo ($lang->index('9')); ?></span>：<?php echo ($Contact["addr"]); ?></li>
	</ul>
	<p><?php echo ($lang->index('12')); ?> <?php echo ($Web["banquan"]); ?></p>
</div>
<script src="/Public/wap/js/swiper-4.1.6.min.js"></script>	
<script src="/Public/wap/js/MobileSiteJs.js"></script>
</body>
</html>
<script type="text/javascript" src="http://api.map.baidu.com/api?key=&v=1.1&services=true"></script>
<script type="text/javascript">
    //创建和初始化地图函数：
    function initMap(){
        createMap();//创建地图
        setMapEvent();//设置地图事件
        addMapControl();//向地图添加控件
    }
    
    //创建地图函数：
    function createMap(){
        var map = new BMap.Map("dituContent");//在百度地图容器中创建一个地图
        var point = new BMap.Point(112.976535,38.520237);//定义一个中心点坐标
        map.centerAndZoom(point,17);//设定地图的中心点和坐标并将地图显示在地图容器中
        window.map = map;//将map变量存储在全局
    }
    
    //地图事件设置函数：
    function setMapEvent(){
        map.enableDragging();//启用地图拖拽事件，默认启用(可不写)
        map.enableScrollWheelZoom();//启用地图滚轮放大缩小
        map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)
        map.enableKeyboard();//启用键盘上下左右键移动地图
    }
    
    //地图控件添加函数：
    function addMapControl(){
                        }
    
    
    initMap();//创建和初始化地图
</script>