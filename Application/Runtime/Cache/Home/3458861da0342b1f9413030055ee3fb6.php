<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首页</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--WebSiteCss-->
	<link rel="stylesheet" href="/Public/index/css/animate.css">
	<!--效果-->
	<link rel="stylesheet" href="/Public/index/css/aos.css" />
	<link rel="stylesheet" href="/Public/index/css/jquery-rebox.css">
	<link rel="stylesheet" href="/Public/index/css/swiper.min.css">
	<link rel="stylesheet" href="/Public/index/css/PcSiteStyle.css">
	<!--WebSiteJs-->
	<script src="/Public/index/js/jquery-2.1.4.min.js"></script>
	<script src="/Public/index/js/jquery-rebox.js"></script>
</head>
<body>
<div class="header-box">
	<div class="header center">
		<div class="logo">
			<a href="<?php echo U('/');?>"><img src="/Public/index/img/logo.png" alt=""></a>
		</div>
		<div class="nav-box">
			<ul class="nav">
				<li class="on"><a href="<?php echo U('Index/index');?>">首页</a></li>
				<li><a href="<?php echo U('About/index');?>">走进瑞德</a></li>
				<li><a href="<?php echo U('Product/index');?>">产品展示</a></li>
				<li><a href="<?php echo U('Tech/index');?>">设备展示</a></li>
				<li><a href="<?php echo U('Quality/index');?>">品质管理</a></li>
				<li><a href="<?php echo U('After/index');?>">售后服务</a></li>
				<li><a href="<?php echo U('Contact/index');?>">联系我们</a></li>
			</ul>
			<div class="nav2">
				<div class="search-box">
					<a href="javascript:;"><img src="/Public/index/img/search.png" alt=""></a>
					<div class="search">
						<div class="search-input">
							<input type="text" class="ipt" placeholder="请输入要搜索的内容">
							<input type="submit" class="tj" value="">
						</div>
					</div>
				</div>
				<div class="nav2-span">
					<span class="cn"><img src="/Public/index/img/ch.png" alt="" />中文</span><em><img src="/Public/index/img/jt.png" alt=""></em>
					<ul class="lan">
						<li><a href="http://testing.com/">中文</a></li>
						<li><a href="http://en.testing.com/">英文</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="swiper-container banner">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<img src="/Public/index/img/banner3.jpg" alt="" />
			<div class="font font3">
				<div class="f1"><img src="/Public/index/img/b-font3.png" alt="" /></div>
				<div class="f4">换热器管板 法兰 锻件 专业制造厂家</div>
				<ul>
					<li><img src="/Public/index/img/b-icon1.png" alt="" /><p>品质保证</p></li>
					<li><img src="/Public/index/img/b-icon2.png" alt="" /><p>优质材料</p></li>
					<li><img src="/Public/index/img/b-icon3.png" alt="" /><p>专业制造</p></li>
					<li><img src="/Public/index/img/b-icon4.png" alt="" /><p>规格多样</p></li>
					<li><img src="/Public/index/img/b-icon5.png" alt="" /><p>库存充足</p></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="center">
	<div class="catposBox">
		<a href="#">首页</a>
		<em>></em>
		<span>产品中心</span>
	</div>
	<div class="proCenter">
		<div class="left">
			<div class="t">产品中心</div>
			<ul>
				<li class="on"><a href="#">管板</a></li>
				<li><a href="#">法兰</a></li>
				<li><a href="#">凸缘</a></li>
				<li><a href="#">锻件</a></li>
				<li><a href="#">堆焊管板</a></li>
				<li><a href="#">折流板</a></li>
				<li><a href="#">异型法兰</a></li>
				<li><a href="#">异形锻件</a></li>
			</ul>
		</div>
		<div class="right">
			<ul class="proList">
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro1.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro2.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro3.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro4.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro5.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro6.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro7.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro8.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
				<li>
					<a href="产品展示-1.html">
						<div class="img">
							<img src="/Public/index/img/pro8.png" alt="" />
						</div>
						<p>活套法兰</p>
					</a>
				</li>
			</ul>
			<div class="pages right">
				<a href="#" class="disabled">上一页</a>
				<span class="current">1</span>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">4</a>
				<a href="#">5</a>
				...
				<a href="#">10</a>
				<a href="#">下一页</a>
			</div>
		</div>
	</div>

</div>

<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: false,
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
</script>
<div class="footer-box">
	<div class="center">
		<div class="left">
			<div class="item logo">
				<img src="/Public/index/img/f-logo.png" alt="" />
			</div>
			<div class="item">
				<p>公司地址：山西省定襄县崔家庄工业区</p>
				<p>邮件：hsx@sx-ruide.com</p>
			</div>
			<div class="item">
				<p>传真：0350-3322998</p>
				<p>电话：13935035615</p>
			</div>
		</div>
		<div class="right">
			版权所有 Copyright(c)2009-2019
		</div>
	</div>
</div>
</body>
<!--效果-->
<script src="/Public/index/js/aos.js"></script>
<script src="/Public/index/js/swiper-4.1.6.min.js"></script>
<script src="/Public/index/js/PcSiteJs.js"></script>
<script>
	AOS.init({
		easing: 'ease-out-back',
		duration: 1000
	});
	new Swiper('.banner', {
		resistanceRatio:0,
		autoplay: {
			disableOnInteraction: false,
		},
		pagination: {
			el: '.banner .swiper-pagination',
		},
		on:{
			init:function(swiper){
				slide=this.slides.eq(0);
				slide.addClass('ani-slide');
			},
			transitionStart: function(){
				for(i=0;i<this.slides.length;i++){
					slide = this.slides.eq(i);
					slide.removeClass('ani-slide');
				}
			},
			transitionEnd: function(){
				slide=this.slides.eq(this.activeIndex);
				slide.addClass('ani-slide');

			},
		}
	});
	var mySwiper = new Swiper('.horner', {
		slidesPerView: 4,
		spaceBetween: 30,
	});
	$('.index-content2 .center .left').click(function(){
		mySwiper.slidePrev();
	})
	$('.index-content2 .center .right').click(function(){
		mySwiper.slideNext();
	})
	$('.horner').rebox({ selector: '.hornerList a' });
</script>
</html>