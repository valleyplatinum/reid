$(function(){
	var admin_tou=$('#admin_tou').val();
	var app=$('#app').val();

	var editor = new wangEditor('content');
	editor.config.menus = $.map(wangEditor.config.menus, function(item, key) {
	    if (item === 'insertcode') {
	        return null;
	    }
	    if (item === 'fullscreen') {
	        return null;
	    }
	    return item;
	});
	editor.config.uploadImgUrl =  app+'/Admin/Bianjiqi/wangEditor';
	editor.create();

	$('#queding').click(function(){
		var title=$.trim($('#title').val());
		var description=$.trim($('#description').val());
		var text=editor.$txt.html();
		var id=$('.id').val();

		if(!id){
			$.post(app+'/Admin/News/insert',{title:title,description:description,text:text},function(data){
				if(data==1){
					var d = dialog({
					    title: admin_tou,
					    content: '添加成功',
					    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
					    ok: function () {}
					});
					d.showModal();
					$('.ui-dialog-button').click(function(){
						location=document.referrer;
					})
				}
			})
		}else{
			$.post(app+'/Admin/News/update',{title:title,description:description,text:text,id:id},function(data){
				if(data==1){
					var d = dialog({
					    title: admin_tou,
					    content: '修改成功',
					    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
					    ok: function () {}
					});
					d.showModal();
					$('.ui-dialog-button').click(function(){
						location=document.referrer;
					})
				}
			})
		}
	})
	
})