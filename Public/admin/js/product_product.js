$(function(){
	var admin_tou=$('#admin_tou').val();
	var app=$('#app').val();

	$('.del').click(function(){
		var id=$(this).attr('index');
		var d = dialog({
		    title: admin_tou,
		    content: '确认删除吗？',
		    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
		    ok: function () {},
		    cancelValue: '<div style="padding-left:15px;padding-right:15px">取消</div>',
		    cancel: function () {}
		});
		d.showModal();
		$('.ui-dialog-autofocus').click(function(){
			$.post(app+'/Admin/Product/product_delete',{id:id},function(data){
				if(data==1){
					location.reload()
				}
			})
		})
	})

	$("#type").change(function(){
	  	var type=$('#type option:selected').val();
	  	location=app+'/Admin/Product/product/type/'+type
	})
	var type=$('.type').val();
	$("#type").find("option[value='"+type+"']").attr("selected",true);

	if(!type || type==0){
		$("#type2").hide();
	}
	$("#type2").change(function(){
		var type=$('#type option:selected').val();
	  	var type2=$('#type2 option:selected').val();
	  	location=app+'/Admin/Product/product/type/'+type+'/type2/'+type2
	});
	var type2=$('.type2').val();
	$("#type2").find("option[value='"+type2+"']").attr("selected",true);
	
})