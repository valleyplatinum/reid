$(function(){

	$('#login').click(function(){
		var url=$('#url').val();
		var username=$.trim($('#username').val());
		var password=$.trim($('#password').val());
		var sms=$.trim($('#sms').val());

		if(username == ''){
			var content="请输入账户";
			dialogs(content);
			$('#username').focus()
			return false;
		}
		
		if(password == ''){
			var content="请输入密码";
			dialogs(content);
			$('#password').focus()
			return false;
		}
		
		if(sms == ''){
			var content="请输入验证码";
			dialogs(content);
			$('#sms').focus()
			return false;
		}

		$.post('login_yan',{username:username,password:password,sms:sms},function(data){
			if(data == 1){
				var content="没有查找到该用户";
				dialogs(content);
				$('#username').focus()
				return false;
			}
			if(data == 2){
				var content="密码错误";
				dialogs(content);
				$('#password').focus()
				return false;
			}
			if(data == 3){
				var content="验证码错误";
				dialogs(content);
				$('#sms').focus()
				return false;
			}
			if(data==4){
				location=url+"/Admin/Index/index";
			}
		})
	})

	document.onkeydown = function(e){  
	    var ev = document.all ? window.event : e;
	    if(ev.keyCode==13) {
	    	if($('.ui-dialog').length>0){
	    		$('.ui-popup-backdrop,.ui-popup').remove();
	    		return false;
	    	}
	    	var url=$('#url').val();
	    	var username=$.trim($('#username').val());
	    	var password=$.trim($('#password').val());
	    	var sms=$.trim($('#sms').val());

	    	if(username == ''){
	    		var content="请输入账户";
	    		dialogs(content);
	    		$('#username').focus()
	    		return false;
	    	}
	    	
	    	if(password == ''){
	    		var content="请输入密码";
	    		dialogs(content);
	    		$('#password').focus()
	    		return false;
	    	}
	    	
	    	if(sms == ''){
	    		var content="请输入验证码";
	    		dialogs(content);
	    		$('#sms').focus()
	    		return false;
	    	}

	    	$.post('login_yan',{username:username,password:password,sms:sms},function(data){
	    		if(data == 1){
	    			var content="没有查找到该用户";
	    			dialogs(content);
	    			$('#username').focus()
	    			return false;
	    		}
	    		if(data == 2){
	    			var content="密码错误";
	    			dialogs(content);
	    			$('#password').focus()
	    			return false;
	    		}
	    		if(data == 3){
	    			var content="验证码错误";
	    			dialogs(content);
	    			$('#sms').focus()
	    			return false;
	    		}
	    		if(data==4){
	    			location=url+"/Admin/Index/index";
	    		}
	    	})
	    }
	}

	$("#verify_img").click(function() {
	    var verifyURL = "verify";
	    var time = new Date().getTime();
	    $("#verify_img").attr({
	       "src" : verifyURL + "/" + time
	    });
	});

	$('#reset').click(function(){
		$('#username').val('');
		$('#password').val('');
		$('#sms').val('');
		$('#username').focus();
	})
	
})

function dialogs(content){
	var admin_tou=$('#admin_tou').val();

	var d = dialog({
	    title: admin_tou,
	    content: content,
	    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
	    ok: function () {},
	});
	d.showModal();
	$("#verify_img").click();
}