$(function(){
	var admin_tou=$('#admin_tou').val();
	var app=$('#app').val();

	$('#queding').click(function(){
		var title=$.trim($('#title').val());
		var text=$.trim($('#text').val());
		var type=$('#type option:selected').val();
		var type2=$('#type2 option:selected').val();
		var id=$('.id').val();

		if(!id){
			$.post(app+'/Admin/Product/product_insert',{title:title,text:text,type:type,type2:type2},function(data){
				if(data==1){
					var d = dialog({
					    title: admin_tou,
					    content: '添加成功',
					    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
					    ok: function () {}
					});
					d.showModal();
					$('.ui-dialog-button').click(function(){
						location=document.referrer;
					})
				}
			})
		}else{
			$.post(app+'/Admin/Product/product_update',{title:title,text:text,type:type,type2:type2,id:id},function(data){
				if(data==1){
					var d = dialog({
					    title: admin_tou,
					    content: '修改成功',
					    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
					    ok: function () {}
					});
					d.showModal();
					$('.ui-dialog-button').click(function(){
						location=document.referrer;
					})
				}
			})
		}
	})

	var type=$('.type').val();
	$("#type").find("option[value='"+type+"']").attr("selected",true);
	var type2=$('.type2').val();
	$("#type2").find("option[value='"+type2+"']").attr("selected",true);

	$("#type").change(function(){
	  	var type=$('#type option:selected').val();
	  	$.post(app+'/Admin/Product/ajax_classify',{type:type},function(data){
	  		$('#type2').html(data)
	  	})
	});
	
})