$(function(){
	var admin_tou=$('#admin_tou').val();
	var url=$('#url').val();

	$('#queding').click(function(){
		var old_pass=$('#old_pass').val();
		var new_pass=$('#new_pass').val();
		var new_pass_len=$('#new_pass').val().length;
		var passReg= !!new_pass.match(/^[a-zA-z]/);
		var repass=$('#repass').val();

		if(old_pass==''){
			$('#old_pass').focus();
			var d = dialog({
			    title: admin_tou,
			    content: '请输入旧密码',
			    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
			    ok: function () {},
			});
			d.showModal();
			return false;
		}
		if(new_pass==''){
			$('#new_pass').focus();
			var d = dialog({
			    title: admin_tou,
			    content: '请输入新密码',
			    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
			    ok: function () {},
			});
			d.showModal();
			return false;
		}
		if(passReg == false){
			$('#new_pass').focus();
			var d = dialog({
			    title: admin_tou,
			    content: '密码须以字母开头',
			    cancel: false,
			    okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
			    ok: function () {}
			});
			d.showModal();
			return false;
		}
		if(new_pass_len < 6){
			var d = dialog({
			    title: admin_tou,
			    content: '新密码长度6至20位',
			    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
			    ok: function () {},
			});
			d.showModal();
			return false;
		}
		if(new_pass != repass){
			$('#repass').focus();
			var d = dialog({
			    title: admin_tou,
			    content: '两次密码不相同',
			    cancel: false,
			    okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
			    ok: function () {}
			});
			d.showModal();
			return false;
		}

		$.post('update_password',{old_pass:old_pass,new_pass:new_pass},function(data){
			if(data==1){
				$('#old_pass').focus();
				var d = dialog({
				    title: admin_tou,
				    content: '旧密码错误',
				    cancel: false,
				    okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
				    ok: function () {}
				});
				d.showModal();
				return false;
			}
			if(data==2){
				var d = dialog({
				    title: admin_tou,
				    content: '修改成功',
				    cancel: false,
				    okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
				    ok: function () {}
				});
				d.showModal();
				$('.ui-dialog-button').click(function(){
					location.reload();
				})
			}
		})
	})
	
})