$(function(){
	var admin_tou=$('#admin_tou').val();
	var url=$('#url').val();

	$('.you').click(function(){
		var name=$(this).attr('index');

		var d = dialog();
		d.showModal();

		$.post(url+'/Admin/System/youhua',{name:name},function(data){
			if(data==1){
				$('.ui-popup-backdrop,.ui-popup').remove();
				var d = dialog({
				    title: admin_tou,
				    content: '优化成功',
				    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
				    ok: function () {},
				});
				d.showModal();
				$('.ui-dialog-button').click(function(){
					location.reload()
				})
			}
		})
	})
	
	$('.xiu').click(function(){
		var name=$(this).attr('index');

		var d = dialog();
		d.showModal();

		$.post(url+'/Admin/System/xiufu',{name:name},function(data){
			if(data==1){
				$('.ui-popup-backdrop,.ui-popup').remove();
				var d = dialog({
				    title: admin_tou,
				    content: '修复成功',
				    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
				    ok: function () {},
				});
				d.showModal();
				$('.ui-dialog-button').click(function(){
					location.reload()
				})
			}
		})
	})

	$('.bei').click(function(){
		var name=$(this).attr('index');

		var d = dialog();
		d.showModal();

		$.post(url+'/Admin/System/beifen',{name:name},function(data){
			if(data==1){
				$('.ui-popup-backdrop,.ui-popup').remove();
				var d = dialog({
				    title: admin_tou,
				    content: '备份成功',
				    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
				    ok: function () {},
				});
				d.showModal();
				$('.ui-dialog-button').click(function(){
					location.reload()
				})
			}
		})
	})

	$('.beifen').click(function(){
		var num_haizi = document.getElementsByName("jobs");
		var cou_haizi = 0;  
		for(var i=0;i<num_haizi.length;i++){  
		    if(num_haizi[i].checked){
		        cou_haizi++;  
		    }  
		}
		if(cou_haizi==0){
			var d = dialog({
			    title: admin_tou,
			    content: '请选择数据表',
			    cancel: false,
			    okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
			    ok: function () {}
			});
			d.showModal();
			return false;
		}

		var obj=document.getElementsByName('jobs');
		var jobs=''; 
		for(var i=0; i<obj.length; i++){ 
		    if(obj[i].checked) jobs+=obj[i].value+','; 
		}

		var d = dialog();
		d.showModal();

		$.post(url+'/Admin/System/beifen',{jobs:jobs},function(data){
			if(data==1){
				$('.ui-popup-backdrop,.ui-popup').remove();
				var d = dialog({
				    title: admin_tou,
				    content: '备份成功',
				    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
				    ok: function () {},
				});
				d.showModal();
				$('.ui-dialog-button').click(function(){
					location.reload()
				})
			}
		})	
	})

	$('.youhua').click(function(){
		var num_haizi = document.getElementsByName("jobs");
		var cou_haizi = 0;  
		for(var i=0;i<num_haizi.length;i++){  
		    if(num_haizi[i].checked){
		        cou_haizi++;  
		    }  
		}
		if(cou_haizi==0){
			var d = dialog({
			    title: admin_tou,
			    content: '请选择数据表',
			    cancel: false,
			    okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
			    ok: function () {}
			});
			d.showModal();
			return false;
		}

		var obj=document.getElementsByName('jobs');
		var jobs=''; 
		for(var i=0; i<obj.length; i++){ 
		    if(obj[i].checked) jobs+=obj[i].value+','; 
		}

		var d = dialog();
		d.showModal();

		$.post(url+'/Admin/System/youhua2',{jobs:jobs},function(data){
			if(data==1){
				$('.ui-popup-backdrop,.ui-popup').remove();
				var d = dialog({
				    title: admin_tou,
				    content: '优化成功',
				    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
				    ok: function () {},
				});
				d.showModal();
				$('.ui-dialog-button').click(function(){
					location.reload()
				})
			}
		})	
	})

	$('.xiufu').click(function(){
		var num_haizi = document.getElementsByName("jobs");
		var cou_haizi = 0;  
		for(var i=0;i<num_haizi.length;i++){  
		    if(num_haizi[i].checked){
		        cou_haizi++;  
		    }  
		}
		if(cou_haizi==0){
			var d = dialog({
			    title: admin_tou,
			    content: '请选择数据表',
			    cancel: false,
			    okValue: '<div style="padding-left:20px;padding-right:20px">确认</div>',
			    ok: function () {}
			});
			d.showModal();
			return false;
		}

		var obj=document.getElementsByName('jobs');
		var jobs=''; 
		for(var i=0; i<obj.length; i++){ 
		    if(obj[i].checked) jobs+=obj[i].value+','; 
		}

		var d = dialog();
		d.showModal();

		$.post(url+'/Admin/System/xiufu2',{jobs:jobs},function(data){
			if(data==1){
				$('.ui-popup-backdrop,.ui-popup').remove();
				var d = dialog({
				    title: admin_tou,
				    content: '修复成功',
				    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
				    ok: function () {},
				});
				d.showModal();
				$('.ui-dialog-button').click(function(){
					location.reload()
				})
			}
		})	
	})

})