$(function(){
	var admin_tou=$('#admin_tou').val();
	var app=$('#app').val();

	$('#queding').click(function(){
		var img=$.trim($('#iimg1').val());
		var title=$.trim($('#title').val());
		var type=$('#type option:selected').val();
		var id=$('.id').val();

		if(!id){
			$.post(app+'/Admin/Tech/insert',{type:type,img:img,title:title},function(data){
				if(data==1){
					var d = dialog({
					    title: admin_tou,
					    content: '添加成功',
					    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
					    ok: function () {}
					});
					d.showModal();
					$('.ui-dialog-button').click(function(){
						location=document.referrer;
					})
				}
			})
		}else{
			$.post(app+'/Admin/Tech/update',{type:type,img:img,id:id,title:title},function(data){
				if(data==1){
					var d = dialog({
					    title: admin_tou,
					    content: '修改成功',
					    okValue: '<div style="padding-left:15px;padding-right:15px">确认</div>',
					    ok: function () {}
					});
					d.showModal();
					$('.ui-dialog-button').click(function(){
						location=document.referrer;
					})
				}
			})
		}
	})

	var type=$('.type').val();
	$("#type").find("option[value='"+type+"']").attr("selected",true);
	
})