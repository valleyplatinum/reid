<?php 
	class lang{
		public function index($num){

			if($_SERVER['SERVER_NAME'] =='www.sx-ruide.com' || $_SERVER['SERVER_NAME'] =='sx-ruide.com'){
				$next=1;
			}elseif($_SERVER['SERVER_NAME'] =='en.sx-ruide.com'){
				$next=0;
			}

			$arr=array(
				'0'=>array('VIEW MORE','查看更多'),
				'1'=>array('Product center','产品中心'),
				'2'=>array('More','更多'),
				'4'=>array('Project cases','项目案例'),
				'5'=>array('Qualification honor','资质荣誉'),
				'6'=>array('Tel','电话'),
				'7'=>array('Fax','传真'),
				'8'=>array('E-mail','邮箱'),
				'9'=>array('Address','地址'),
				'10'=>array('Search','搜索'),
				'11'=>array('Please enter what you want to search for','请输入要搜索的内容'),
				'12'=>array('All rights reserved','版权所有'),
				'13'=>array('Chinese','中文'),
				'14'=>array('English','英文'),
				'15'=>array('Share to WeChat','分享到微信'),
				'16'=>array('Share to QQ friends','分享到QQ好友'),
				'17'=>array('share to Sina Weibo','分享到新浪微博'),
				'18'=>array('Share to Tencent Weibo','分享到腾讯微博'),
				'19'=>array('Share to QQ space','分享到QQ空间'),
				'20'=>array('Home','首页'),
				'21'=>array('TEAM BUILDING','团队建设'),
				'22'=>array('FACTORY APPEARANCE','厂区厂貌'),
				'23'=>array('Standard Flange','标准法兰'),
				'24'=>array('Forgings','锻件'),
				'25'=>array('Non-Standard Flange','非标准法兰'),
				'26'=>array('Production equipment','生产设备'),
				'27'=>array('Testing Equipment','检测设备'),
				'28'=>array('Non-destructive equipment','无损设备'),
				'29'=>array('Manager','总经理'),
				'30'=>array('Date','发布日期'),
				'31'=>array('Views','浏览次数'),
				'32'=>array('Latest reading','最新导读'),
				'33'=>array('Related Reading','相关阅读'),
				'34'=>array('Shanxi Reid application field','山西瑞德应用领域'),
				'35'=>array('Not retrieved','没有检索到')
				);
			return $arr[$num][$next];
		}	
	}
?>